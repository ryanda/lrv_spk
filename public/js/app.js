Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('#token').getAttribute('value');
// Vue.config({
// 	'debug': true
// });
Vue.config.debug = true;
new Vue({
    el: '#rest',
    ready: function() {
        setTimeout(this.fetchData, 500);
    },
    data: {
        newLokasi: {
            alamat: '',
            kbp: '',
            prv: '',
            lat: '',
            lon: ''
        },
        lokasi: {
            noid: '',
            alamat: '',
            kbp: '',
            prv: '',
            lat: '',
            lon: ''
        },
        sortKey: 'no',
        sortRvs: true,
        columns: ['no', 'noid', 'alamat', 'kabupaten', 'provinsi'],
        search: '',
        jmldata : 0,
        data : {},
        detail : {}
    },
    methods: {
        
        // READ
        fetchData: function() {
            $.ajax({
                type: 'GET',
                context: this,
                url: '/api/',
                contentType: 'application/json',
                success: function(result) {
                    $('#ajax').hide();
                    this.$set('data', result.lokasi);
                    this.$set('jmldata', result.lokasi.length);
                },
                error: function(req, status, ex) {
                    Materialize.toast('Data gagal diload! Status : ' + status + ', Data : ' + ex, 10000);
                },
            });
        },
        detailModal: function(data) {
            $.ajax({
                type: 'GET',
                context: this,
                url: '/api/' + data.id,
                contentType: 'application/json',
                success: function(result) {
                    this.$set("detail", result.lokasi);
                    $('#detail').openModal();
                },
                error: function(req, status, ex) {
                    Materialize.toast('Data gagal diload! ' + status + ' ' + ex, 10000);
                },
            });
        },

        // DELETE
        onDeleteForm: function(data, e) {
            e.preventDefault();
            this.lokasi = data;
            $.ajax({
                type: 'DELETE',
                url: '/api/' + data.id,
                contentType: 'application/json',
                beforeSend: function() {
                    Materialize.toast('Data sedang dihapus', 500);
                },
                success: function() {
                    Materialize.toast('Data' + data['alamat'] + ' berhasil dihapus!', 2000);
                    $('#hapus').closeModal();
                },
                error: function(req, status, ex) {
                    Materialize.toast('Komunikasi dengan server gagal! ' + status + ' ' + ex, 10000);
                },
            });
            this.data.$remove(data);
        },
        deleteModal: function(data) {
            this.lokasi = data;
            $('#hapus').openModal();
        },
        
        // CREATE
        onSubmitForm: function(e) {
            e.preventDefault();
            lokasi = this.newLokasi;
            $.ajax({
                type: 'POST',
                url: '/api/',
                dataType: 'text',
                processData: false,
                data: JSON.stringify(lokasi),
                contentType: 'application/json',
                success: function() {
                    Materialize.toast('Data ' + lokasi.alamat + ' berhasil ditambah! :D', 1000);
                },
                error: function(req, status, ex) {
                    Materialize.toast('Komunikasi dengan server gagal! ' +req+ ' ' +status+ ' ' +ex, 10000);
                },
            });
            this.data.push(lokasi);
            this.fetchData();
            this.newLokasi = {
                alamat: '',
                kbp: '',
                prv: '',
                lat: '',
                lon: ''
            };
            $('#tambah').closeModal();
        },

        // EDIT
        onUpdateForm: function(lokasi, e) {
            e.preventDefault();
            $.ajax({
                type: 'PUT',
                url: '/api/' + lokasi.id,
                data: lokasi,
                success: function(data) {
                    Materialize.toast('Data '+lokasi.alamat+' berhasil di ubah! ' + data.message, 1000);
                    $('#edit').closeModal();
                },
                error: function(req, status, ex) {
                    Materialize.toast('Komunikasi dengan server gagal! ' + status + ' ' + ex, 10000);
                },
            });
            // this.fetchData();
            $('#edit').closeModal();
        },
        editModal: function(data) {
            this.lokasi = data;
            $('#edit').openModal();
        },

        // FUNCTIONS
        sortBy: function(sortKey) {
            this.sortRvs = (this.sortKey == sortKey) ? !this.sortRvs : false;
            this.sortKey = sortKey;
        },
    },
    computed: {
        errors: function() {
            for (var key in this.newLokasi) {
                if (! this.newLokasi[key]) return true;
            }
            return false;
        }
    }
});

new Vue({
    el: '#tasks',
    data: {
        tasks: ['browsing', 'reading', 'listening']
    },
    methods: {
        addTask: function() {
            this.tasks.push(this.newTask);
            this.newTask = ' ';
        }
    }
});

// new Vue({
//     el: '#content',
//     data: {
//         markdownInput: "# Main Heading Here - Headings use '#'s \n### Sub Heading Here\n\n---\n\nMain info here - Paragraphs are just written as plain text\n\n---\n`code - Code uses '``'`\n\n ---\n\n[This is a link](http://google.com)\n\n Links use '[]' for the text portion and then '()' for the link portion\n\n---\n\n#### Raw html can also be used\n\n<ul><li>List item</li><li>Another list item</li></ul>\n\n---\n\n#### Images can also be used\nImages use '![ALT_TEXT]' at the start to say it's an image, follow with no spaces by '(IMAGE_LINK)'\n\n![](http://pngj.googlecode.com/git-history/2edf695f26ea116e37912a6b2c7f46b3c91e1a37/pngj/src/test/resources/testcomp/04-c2-filtered.png?r=2edf695f26ea116e37912a6b2c7f46b3c91e1a37)"
//     },
//     filters: {
//         marked: 'marked'
//     }
// });