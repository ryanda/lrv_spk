<?php

class NilaiTableSeeder extends  Seeder {
    public function run() {
        // DB::table('users')->truncate();
        
        $nilai = [
                    [
                        'krt'   => 'Kepadatan Lalu Lintas',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah SPBU tiap ruas Jalan',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perumahan per Kecamatan',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perkantoran dan Industri',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    [
                        'krt'   => 'Kepadatan Penduduk',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    [
                        'krt'   => 'Harga Lahan Setempat',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1,
                        'prt'   => 1
                    ],
                    
                ];

        foreach($nilai as $data){
            Nilai::create($data);
        }
    }
}