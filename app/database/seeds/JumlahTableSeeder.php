<?php

class JumlahTableSeeder extends  Seeder {
    public function run() {
        DB::table('jml_baris')->truncate();
        
        $jml = [
                    [
                        'krt'   => 'Kepadatan Lalu Lintas',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah SPBU tiap ruas Jalan',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perumahan per Kecamatan',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perkantoran dan Industri',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    [
                        'krt'   => 'Kepadatan Penduduk',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    [
                        'krt'   => 'Harga Lahan Setempat',
                        'k1'    => 1,
                        'k2'    => 1,
                        'k3'    => 1,
                        'k4'    => 1,
                        'k5'    => 1,
                        'k6'    => 1,
                        'jml'   => 1
                    ],
                    
                ];

        foreach($jml as $data){
            Jumlah::create($data);
        }
    }
}