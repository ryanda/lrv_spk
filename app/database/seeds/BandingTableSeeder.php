<?php

use Faker\Factory as Faker;

class BandingTableSeeder extends  Seeder {
    public function run() {
        DB::table('krt_banding')->truncate();
        $faker = Faker::create();
        $bdg = [
                    [
                        'krt'   => 'Kepadatan Lalu Lintas',
                        'k1'    => 1,
                        'k2'    => $b21 = $faker->randomNumber(1, 5),
                        'k3'    => $b31 = $faker->randomNumber(1, 5),
                        'k4'    => $b41 = $faker->randomNumber(1, 5),
                        'k5'    => $b51 = $faker->randomNumber(1, 5),
                        'k6'    => $b61 = $faker->randomNumber(1, 5)
                    ],
                    [
                        'krt'   => 'Jumlah SPBU tiap ruas Jalan',
                        'k1'    => 1/$b21,
                        'k2'    => 1,
                        'k3'    => $b32 = $faker->randomNumber(1, 5),
                        'k4'    => $b42 = $faker->randomNumber(1, 5),
                        'k5'    => $b52 = $faker->randomNumber(1, 5),
                        'k6'    => $b62 = $faker->randomNumber(1, 5)
                    ],
                    [
                        'krt'   => 'Jumlah Perumahan per Kecamatan',
                        'k1'    => 1/$b31,
                        'k2'    => 1/$b32,
                        'k3'    => 1,
                        'k4'    => $b43 = $faker->randomNumber(1, 5),
                        'k5'    => $b53 = $faker->randomNumber(1, 5),
                        'k6'    => $b63 = $faker->randomNumber(1, 5)
                    ],
                    [
                        'krt'   => 'Jumlah Perkantoran dan Industri',
                        'k1'    => 1/$b41,
                        'k2'    => 1/$b42,
                        'k3'    => 1/$b43,
                        'k4'    => 1,
                        'k5'    => $b54 = $faker->randomNumber(1, 5),
                        'k6'    => $b64 = $faker->randomNumber(1, 5)
                    ],
                    [
                        'krt'   => 'Kepadatan Penduduk',
                        'k1'    => 1/$b51,
                        'k2'    => 1/$b52,
                        'k3'    => 1/$b53,
                        'k4'    => 1/$b54,
                        'k5'    => 1,
                        'k6'    => $b65 = $faker->randomNumber(1, 5)
                    ],
                    [
                        'krt'   => 'Harga Lahan Setempat',
                        'k1'    => 1/$b61,
                        'k2'    => 1/$b62,
                        'k3'    => 1/$b63,
                        'k4'    => 1/$b64,
                        'k5'    => 1/$b65,
                        'k6'    => 1
                    ],
                ];

        foreach($bdg as $data){
            Banding::create($data);
        }
    }
}