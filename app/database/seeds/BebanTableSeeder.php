<?php

class BebanTableSeeder extends  Seeder {
    public function run() {
        DB::table('beban')->truncate();
        
        $beban = [
                    [
                        'krt'   => 'Kepadatan Lalu Lintas',
                        'beban'    => 1 
                    ],
                    [
                        'krt'   => 'Jumlah SPBU tiap ruas Jalan',
                        'beban'    => 2
                    ],
                    [
                        'krt'   => 'Jumlah Perumahan per Kecamatan',
                        'beban'    => 3
                    ],
                    [
                        'krt'   => 'Jumlah Perkantoran dan Industri',
                        'beban'    => 4
                    ],
                    [
                        'krt'   => 'Kepadatan Penduduk',
                        'beban'    => 5
                    ],
                    [
                        'krt'   => 'Harga Lahan Setempat',
                        'beban'    => 6
                    ],
                    
                ];

        foreach($beban as $data){
            Beban::create($data);
        }
    }
}