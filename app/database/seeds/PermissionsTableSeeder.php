<?php
class PermissionsTableSeeder extends Seeder {
    public function run()
    {
        if (App::environment() == ('heroku' OR 'linux')) {
//            DB::statement("TRUNCATE TABLE permission_role");
  //          DB::statement("TRUNCATE TABLE permissions CASCADE");
        } else {
            DB::table('permission_role')->truncate();
			DB::table('permissions')->truncate();
        }        

        $permissions = array(
            array( // 1
                'name'         => 'adm',
                'display_name' => 'Admin',
                'created_at'   => new DateTime(),
                'updated_at'   => new DateTime()
            ),
            array( // 2
                'name'         => 'usr',
                'display_name' => 'User',
                'created_at'   => new DateTime(),
                'updated_at'   => new DateTime()
            ),
            array( // 3
                'name'         => 'mng',
                'display_name' => 'Manager',
                'created_at'   => new DateTime(),
                'updated_at'   => new DateTime()
            ),
        );

        // foreach($permissions as $permission){
        //     Permission::create($permission);
        // }

        DB::table('permissions')->insert( $permissions );


        $manager = Role::where('name', '=', 'Manager')->first()->id;
        $user = Role::where('name', '=', 'User')->first()->id;
        $admin = Role::where('name', '=', 'Admin')->first()->id;

        $adm = Permission::where('name', '=', 'adm')->first()->id;
        $mng = Permission::where('name', '=', 'mng')->first()->id;
        $usr = Permission::where('name', '=', 'usr')->first()->id;

        $permission_role = array(
            array(
                'role_id'       => $manager,
                'permission_id' => $mng
            ),
            array(
                'role_id'       => $user,
                'permission_id' => $usr
            ),
            array(
                'role_id'       => $admin,
                'permission_id' => $adm
            ),
        );
        DB::table('permission_role')->insert( $permission_role );
    }
}