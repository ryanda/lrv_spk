<?php

class UserTableSeeder extends  Seeder {
    public function run() {
        if (App::environment() == ('heroku' OR 'linux')) {
  //          DB::statement("TRUNCATE TABLE users CASCADE");
        } else {
            DB::table('users')->truncate();
        }
        
		$pass = '123456';
		
        $users = [
                    [
                        'name' => 'ryandaputra',
                        'username' => 'ryand',
                        'password' => $pass,
                        'file' => 'ryandaputra.jpg'
                    ],
                    [
                        'name' => 'falachsobri',
                        'username' => 'sobri',
                        'password' => $pass,
                        'file' => 'falachsobri.jpg'
                    ],
                    [
                        'name' => 'abdulpiqri',
                        'username' => 'piqri',
                        'password' => $pass,
                        'file' => 'abdulpiqri.jpg'
                    ],
                    [
                        'name' => 'mizwar',
                        'username' => 'mizwar',
                        'password' => $pass,
                        'file' => 'mizwar.jpg'
                    ]
                ];

        foreach($users as $user){
            User::create($user);
        }
    }
}