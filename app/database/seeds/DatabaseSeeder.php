<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->command->info('app environment is... '.App::environment()); 

 // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		// for account login
		$this->call('UserTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('PermissionsTableSeeder');

		// for data
		$this->call('LokasiTableSeeder');
		$this->call('NilaiHasilTableSeeder');

        // for criteria
		$this->call('NilaiTableSeeder');
		$this->call('JumlahTableSeeder');
		$this->call('BandingTableSeeder');
		$this->call('RasioTableSeeder');
		$this->call('HasilTableSeeder');
		$this->call('BebanTableSeeder');



 // DB::statement('SET FOREIGN_KEY_CHECKS=1;');

	}

}
