<?php

class HasilTableSeeder extends  Seeder {
    public function run() {
        // DB::table('users')->truncate();
        
        $nilai = [
                    [
                        'keterangan'   => 'j',
                        'nilai'   => 1
                    ],
                    [
                        'keterangan'   => 'n',
                        'nilai'   => 6
                    ],
                    [
                        'keterangan'   => 'm',
                        'nilai'   => 1
                    ],
                    [
                        'keterangan'   => 'ci',
                        'nilai'   => 1
                    ],
                    [
                        'keterangan'   => 'ir',
                        'nilai'   => 1.24
                    ],
                    [
                        'keterangan'   => 'cr',
                        'nilai'   => 1
                    ],
                    
                    
                ];

        foreach($nilai as $data){
            Hasil::create($data);
        }
    }
}