<?php
class RolesTableSeeder extends Seeder {
    public function run()
    {
        if (App::environment() == ('heroku' OR 'linux')) {
//            DB::statement("TRUNCATE TABLE assigned_roles");
  //          DB::statement("TRUNCATE TABLE roles CASCADE");
        } else {
            DB::table('assigned_roles')->truncate();
            DB::table('roles')->truncate();
        }        

        $admin = new Role;
        $admin->name = 'Admin';
        $admin->save();
      
        $user = new Role;
        $user->name = 'User';
        $user->save();

        $manager = new Role;
        $manager->name = 'Manager';
        $manager->save();

        $user1 = User::where('username','=','ryand')->first();
        $user1->attachRole($manager);
        $user2 = User::where('username','=','sobri')->first();
        $user2->attachRole($user);
        $user3 = User::where('username','=','piqri')->first();
        $user3->attachRole($admin);
        $user4 = User::where('username','=','mizwar')->first();
        $user4->attachRole($admin);
    }
}