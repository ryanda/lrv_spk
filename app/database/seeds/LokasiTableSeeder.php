<?php

use Faker\Factory as Faker;

class LokasiTableSeeder extends Seeder {
    public function run()
    {
        $faker = Faker::create();


        Lokasi::truncate();

        foreach(range(1, 20) as $index)
        {
            Lokasi::create([
                'noid' => $faker->numerify('####'),
                'alamat' => $faker->address,
                'kbp' => $faker->city,
                'prv' => $faker->state,
                'lat' => $faker->latitude,
                'lon' => $faker->longitude
            ]);
        }
    }
}
