<?php

use Faker\Factory as Faker;

class NilaiHasilTableSeeder extends Seeder {
    public function run()
    {
        $faker = Faker::create();
        NilaiData::truncate();
        HasilSaw::truncate();
        HasilAhp::truncate();
        $krt = DB::table('krt_nilai')->lists('prt');

        foreach(range(1, 20) as $index)
        {
            $n1 = $faker->randomNumber(1, 5);
            $n2 = $faker->randomNumber(1, 5);
            $n3 = $faker->randomNumber(1, 5);
            $n4 = $faker->randomNumber(1, 5);
            $n5 = $faker->randomNumber(1, 5);
            $n6 = $faker->randomNumber(1, 5);

            NilaiData::create([
                'lokasi_id' => $index,
                'krt1' => $n1,
                'krt2' => $n2,
                'krt3' => $n3,
                'krt4' => $n4,
                'krt5' => $n5,
                'krt6' => $n6,
                'nilai' => 1
            ]);

            HasilAhp::create([
                'nilai_id' => $index,
                'lokasi_id' => $index,
                'k1' => $n1,
                'k2' => $n2,
                'k3' => $n3,
                'k4' => $n4,
                'k5' => $n5,
                'k6' => $n6
            ]);

            HasilSaw::create([
                'nilai_id' => $index,
                'lokasi_id' => $index,
                'k1' => $n1,
                'k2' => $n2,
                'k3' => $n3,
                'k4' => $n4,
                'k5' => $n5,
                'k6' => $n6
            ]);
        }
    }
}
