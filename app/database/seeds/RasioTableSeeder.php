<?php

class RasioTableSeeder extends  Seeder {
    public function run() {
        // DB::table('users')->truncate();
        
        $nilai = [
                    [
                        'krt'   => 'Kepadatan Lalu Lintas',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah SPBU tiap ruas Jalan',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perumahan per Kecamatan',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    [
                        'krt'   => 'Jumlah Perkantoran dan Industri',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    [
                        'krt'   => 'Kepadatan Penduduk',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    [
                        'krt'   => 'Harga Lahan Setempat',
                        'jml'   => 1,
                        'prt'   => 1,
                        'hsl'   => 1
                    ],
                    
                ];

        foreach($nilai as $data){
            Rasio::create($data);
        }
    }
}