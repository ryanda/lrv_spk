<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRasioKonsistensi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('rso_kons', function(Blueprint $t) {
			$t->increments('id');
			$t->string('krt');
			$t->double('jml',7,4);
			$t->double('prt',7,4);
			$t->double('hsl',7,4);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('rso_kons');
	}

}
