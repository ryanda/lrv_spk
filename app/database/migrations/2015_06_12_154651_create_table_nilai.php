<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNilai extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('nilai', function(Blueprint $t) {
			$t->increments('id');
			$t->integer('lokasi_id');
			$t->enum('krt1', [1,2,3,4,5])->default(1);
			$t->enum('krt2', [1,2,3,4,5])->default(1);
			$t->enum('krt3', [1,2,3,4,5])->default(1);
			$t->enum('krt4', [1,2,3,4,5])->default(1);
			$t->enum('krt5', [1,2,3,4,5])->default(1);
			$t->enum('krt6', [1,2,3,4,5])->default(1);
			$t->float('nilai')->nullable();
			$t->string('status')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('nilai');
	}

}
