<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHasilSaw extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('hasil_saw', function(Blueprint $t) {
			$t->increments('id');
			$t->integer('nilai_id');
			$t->integer('lokasi_id');
			$t->double('k1',7,4)->default(1);
			$t->double('k2',7,4)->default(1);
			$t->double('k3',7,4)->default(1);
			$t->double('k4',7,4)->default(1);
			$t->double('k5',7,4)->default(1);
			$t->double('k6',7,4)->default(1);
			$t->string('keterangan')->nullable();
			$t->double('nilai',7,4)->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('hasil_saw');
	}

}
