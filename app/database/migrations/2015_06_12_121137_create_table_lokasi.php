<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLokasi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('lokasi', function(Blueprint $t) {
			$t->increments('id');
			$t->string('noid');
			$t->string('alamat', 200);
			$t->string('kbp', 200);
			$t->string('prv', 200);
			$t->double('lat', 15, 8)->nullable();
			$t->double('lon', 15, 8)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lokasi');
	}

}
