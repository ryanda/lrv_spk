<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerbandinganKriteria extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('krt_banding', function(Blueprint $t) {
			$t->increments('id');
			$t->string('krt');
			$t->double('k1',7,4);
			$t->double('k2',7,4);
			$t->double('k3',7,4);
			$t->double('k4',7,4);
			$t->double('k5',7,4);
			$t->double('k6',7,4);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('krt_banding');
	}

}
