<?php

class Jumlah extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'jml_baris';
	public $timestamps = false;
	protected $hidden = ['id', 'krt'];
}