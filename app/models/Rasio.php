<?php

class Rasio extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'rso_kons';
	public $timestamps = false;
	protected $hidden = ['id', 'krt'];
	protected $fillable = ['k1', 'k2', 'k3', 'k4', 'k5', 'k6'];
}