<?php

class HasilSaw extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'hasil_saw';
	public $timestamps = false;
	protected $hidden = ['id'];
	protected $fillable = ['nilai_id', 'lokasi_id', 'k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'keterangan', 'nilai'];


	public function lokasi() {
		return $this->belongsTo('Lokasi');
	}
}