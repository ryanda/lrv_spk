<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, HasRole;
	
	protected $table = 'users';
	protected $hidden = ['password', 'remember_token'];
	protected $fillable = ['name', 'username', 'password'];
	public $timestamps = false;

	public function roles() {
		return $this->belongsToMany('Role', 'assigned_roles');
	}
}