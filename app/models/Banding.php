<?php

class Banding extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'krt_banding';
	public $timestamps = false;
	protected $hidden = ['id', 'krt'];
	protected $fillable = ['k1', 'k2', 'k3', 'k4', 'k5', 'k6'];
}