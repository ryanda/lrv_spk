<?php

class Lokasi extends Eloquent
{
	public $timestamps = false;
	protected $guarded = ['id'];
	protected $table = 'lokasi';
	protected $hidden = [];
	protected $fillable = ['noid', 'alamat', 'kbp', 'prv', 'lat', 'lon'];

	public function nilai() {
		return $this->hasOne('NilaiData');
	}
}