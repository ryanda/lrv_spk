<?php

class Nilai extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'krt_nilai';
	public $timestamps = false;
	protected $hidden = ['id', 'krt'];
	protected $fillable = ['krt', 'k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'jml', 'prt'];


	public function lokasi() {
		$this->belongsTo('Lokasi');
	}
}