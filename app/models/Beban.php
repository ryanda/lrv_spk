<?php

class Beban extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'beban';
	public $timestamps = false;
	protected $hidden = ['id'];
	protected $fillable = ['beban'];
}