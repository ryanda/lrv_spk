<?php

class NilaiData extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'nilai';
	public $timestamps = false;
	protected $hidden = ['id'];
	protected $fillable = ['lokasi_id', 'krt1', 'krt2', 'krt3', 'krt4', 'krt5', 'krt6', 'nilai', 'status'];


	public function lokasi() {
		return $this->belongsTo('Lokasi');
	}

	public function hasil() {
		return $this->hasOne('HasilData');
	}
}