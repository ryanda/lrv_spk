<?php

class Hasil extends Eloquent
{
	protected $guarded = ['id'];
	protected $table = 'hsl_prht';
	public $timestamps = false;
	protected $hidden = ['id', 'keterangan'];
	protected $fillable = ['keterangan', 'nilai'];
}