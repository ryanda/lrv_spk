<?php

//test ajax
Route::get('rest', function() {
    return View::make('test.rest');
});
Route::resource('api', 'TestController');

// for guest only
Route::group(['before' => 'guest'], function() {
    Route::get('login', ['as' => 'home', 'uses' => 'LoginController@login']);
    Route::post('/', 'LoginController@handleLogin');
});

// core app
Route::group(['before' => 'auth'], function() {
    Route::get('/', ['as' => 'filter', 'uses' => 'LoginController@home']);
    Route::group(['prefix' => 'lokasi'], function() {
        Route::get('/', ['as' => 'lokasi', 'uses' => 'LokasiController@index']);
        Route::get('detail/{id}', ['as' => 'lokasidetail', 'uses' => 'LokasiController@detail']);
        Route::get('edit/{id}', ['as' => 'lokasiedit', 'uses' => 'LokasiController@edit']);
        Route::put('edit/{id}',['as'=>'editlokasi','uses'=>'LokasiController@postEdit']);
        Route::get('hapus/{id}', ['as' => 'lokasihapus', 'uses' => 'LokasiController@hapus']);
        Route::get('tambah', ['as' => 'lokasitambah', 'uses' => 'LokasiController@tambah']);
        Route::post('tambah', ['as' => 'tambahlokasi', 'uses' => 'LokasiController@postTambah']);
    });
    Route::group(['prefix' => 'nilai'], function() {
        Route::get('/', ['as' => 'nilai', 'uses' => 'NilaiController@index']);   
        Route::get('detail/{id}', ['as' => 'nilaidetail', 'uses' => 'NilaiController@detail']); 
        Route::get('edit/{id}', ['as' => 'nilaiedit', 'uses' => 'NilaiController@edit']);
        Route::put('edit/{id}',['as'=>'editnilai','uses'=>'NilaiController@postEdit']);
        Route::get('hapus/{id}', ['as' => 'nilaihapus', 'uses' => 'NilaiController@hapus']);
    });

// laporan
    Route::group(['prefix' => 'laporan'], function() {
        Route::get('/', ['as' => 'laporan', 'uses' => 'LaporanController@index']);
        Route::get('detail/{id}', ['as' => 'laporandetail', 'uses' => 'LaporanController@detail']);
        Route::get('hapus/{id}', ['as' => 'laporanhapus', 'uses' => 'LaporanController@hapus']);
    });
    Route::group(['prefix' => 'laporansaw'], function() {
        Route::get('/', ['as' => 'laporsaw', 'uses' => 'LaporanController@indexsaw']);
        Route::get('hitung', ['as' => 'hitungsaw', 'uses' => 'LaporanController@hitungsaw']);
        Route::post('hitung', ['as' => 'hitungpostsaw', 'uses' => 'LaporanController@postHitungsaw']);
    });
    Route::group(['prefix' => 'laporanahp'], function() {
        Route::get('/', ['as' => 'laporahp', 'uses' => 'LaporanController@indexahp']);
        Route::get('hitung', ['as' => 'hitungahp', 'uses' => 'LaporanController@hitungahp']);
        Route::post('hitung', ['as' => 'hitungpostahp', 'uses' => 'LaporanController@postHitungahp']);
    });

// hitung
    Route::group(['prefix' => 'ahp'], function() {
        Route::get('/', ['as' => 'ahp', 'uses' => 'AhpController@index']);
        Route::get('ubah', ['as' => 'ahpubah', 'uses' => 'AhpController@ubah']);
        Route::put('simpan', ['as' => 'ahpsimpan', 'uses' => 'AhpController@hitung']);
    });
    Route::group(['prefix' => 'saw'], function() {
        Route::get('/', ['as' => 'saw', 'uses' => 'SawController@index']);
        Route::get('ubah', ['as' => 'sawubah', 'uses' => 'SawController@ubah']);
        Route::put('simpan', ['as' => 'sawsimpan', 'uses' => 'SawController@hitung']);
    });

// list user
    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
    Route::group(['prefix' => 'users'], function() {
        Route::get('/', ['uses' => 'LoginController@all']);
        Route::get('register', ['as'=>'addusers', 'uses'=>'LoginController@create', 'before' => 'yadm']);
        Route::post('register', ['as' => 'postusers', 'uses' => 'LoginController@store', 'before' => 'yadm']);
    });
});


//dummy
Route::get('testaja', function() {
    return View::make('test');
});

