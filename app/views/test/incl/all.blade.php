<div class="input-field">
    <i class="mdi-action-search prefix"></i>
    <input id="icon_prefix" type="text" class="validate" v-model="search">
    <label for="icon_prefix">Cari Lokasi</label>
</div>

<table class="bordered striped">
    <thead>
        <tr>
        	<td v-repeat="column: columns" v-on="click: sortBy(column)" v-class="actived: sortKey == column">
        		@{{column | uppercase}}<i class="mdi-action-autorenew"></i>
        	</td>
            <td>DETAIL, terdapat <strong>@{{jmldata}} data </strong></td>
        </tr>
    </thead>
    <tbody>

        {{-- ajax loader --}}
        <tr id="ajax">
            <td colspan="6"> 
                <div class="loader"></div> 
            </td>
        </tr>  

        {{-- the datas --}}
        <tr v-repeat="datas : data | filterBy search | orderBy sortKey sortRvs" v-transition="expand" stagger="100">
            <td> @{{datas.id}} </td>
			{{-- <td> @{{$index+1}} </td> --}}
            <td> @{{datas.noid}}</td>
            <td> @{{datas.alamat}}</td>
            <td> @{{datas.kbp}}</td>
            <td> @{{datas.prv}}</td>
            <td>
				<a class="waves-effect waves-light btn teal accent-1" v-on="click:detailModal(datas)"><i class="mdi-content-send"></i></a>
				<a class="waves-effect waves-light btn amber accent-1" v-on="click:editModal(datas)"><i class="mdi-content-create"></i></a>
				<a class="waves-effect waves-light btn red accent-1" v-on="click:deleteModal(datas)"><i class="mdi-action-delete"></i></a>
            </td>
    	</tr>

    </tbody>
</table>