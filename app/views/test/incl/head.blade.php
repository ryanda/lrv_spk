<title>AJAX Test</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta id="token" name="token" value="@{{ csrf_token() }}">
{{HTML::style('css/materialize.min.css')}}
{{HTML::style('css/ani.css')}}
<style type="text/css">
	body {
		display: flex;
		min-height: 100vh;
		flex-direction: column;
	}
	main {
		flex: 1 0 auto;
	}
	.container{width:90%}            
	#ubah tr td { width: 14%} 
	.actived { font-weight: bolder; } 
	td .btn{
		padding: 0 1rem;
	}
	#tasks p {
		margin-left: 10px; 
		padding: 10px 0 10px 0;
	}
</style>