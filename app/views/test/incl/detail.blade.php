<div id="detail" class="modal modal-fixed-footer">
    <div class="modal-content"> <!-- v-model="detail" -->
		<h3 class="center-align">Data Lokasi</h3>
		<p class="flow-text"><i class="mdi-navigation-check left"></i><strong>ID : </strong>@{{detail.noid}}</p>
		<p class="flow-text"><i class="mdi-maps-directions left"></i><strong>Alamat :  </strong>@{{detail.alamat}}</p>
		<p class="flow-text"><i class="mdi-social-domain left"></i><strong>Kabupaten : </strong>@{{detail.kbp}}</p>
		<p class="flow-text"><i class="mdi-social-location-city left"></i><strong>Provinsi : </strong>@{{detail.prv}}</p>
		<p class="flow-text"><i class="mdi-maps-place left"></i><strong>Posisi : </strong>@{{detail.lat}}, @{{detail.lon}}</p>
    </div>
    <div class="modal-footer">
      	<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Tutup</a>
    </div>
</div>