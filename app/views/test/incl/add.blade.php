<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a href="#tambah" class="modal-trigger btn-floating btn-large waves-effect waves-light deep-orange">
		<i class="mdi-content-add"></i>
	</a>
</div>
<div id="tambah" class="modal modal-fixed-footer"> <!-- v-model="lokasi" -->
    <form method="POST" v-on="submit: onSubmitForm">
	    <div class="modal-content">
			<div class="input-field">
                <i class="mdi-maps-directions prefix"></i>
                <label for="alamat" class="active">Alamat</label>
                <input name="alamat" type="text" id="alamat" v-model="newLokasi.alamat">
            </div>
            <div class="input-field">
                <i class="mdi-social-domain prefix"></i>
                <label for="kbp" class="active">Kabupaten</label>
                <input name="kbp" type="text" id="kbp" v-model="newLokasi.kbp">
            </div>
            <div class="input-field">
                <i class="mdi-social-location-city prefix"></i>
                <label for="prv" class="active">Provinsi</label>
                <input name="prv" type="text" id="prv" v-model="newLokasi.prv">
            </div>
            <div class="input-field">
                <i class="mdi-maps-place prefix"></i>
                <label for="lat" class="active">Koordinat Latitude</label>
                <input name="lat" type="text" id="lat" v-model="newLokasi.lat">
            </div>
            <div class="input-field">
                <i class="mdi-maps-place prefix"></i>
                <label for="lon" class="active">Koordinat Longitude</label>
                <input name="lon" type="text" id="lon" v-model="newLokasi.lon">
            </div>
	    </div>
	    <div class="modal-footer">
	      	<a href="#!" class=" modal-action modal-close waves-effect waves-green btn">Batal</a>
	      	<button class="btn waves-effect waves-light" type="submit" name="action" v-attr="disabled:errors">Tambah
	                <i class="mdi-content-send right"></i>
	        </button>
	    </div>
    </form>
</div>