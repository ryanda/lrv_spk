<div id="hapus" class="modal modal-fixed-footer" style="max-height: 350px"> <!--  v-model="lokasi" -->
    <div class="modal-content">
    	<div class="center-align">
			<h3>Anda ingin menghapus data ini?</h3>
			<p class="flow-text"><i class="mdi-navigation-check"></i>ID : <strong>@{{lokasi.noid}}</strong></p>
			<p class="flow-text"><i class="mdi-maps-directions"></i>Alamat :  <strong>@{{lokasi.alamat}}</strong></p>
		</div>
    </div>
    <div class="modal-footer">
      	<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Tutup</a>
      	<a class="waves-effect waves-light btn red" v-on="click:onDeleteForm(lokasi, $event)">Hapus<i class="mdi-action-delete right"></i></a>
    </div>
</div>