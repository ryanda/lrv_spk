<div id="tasks">
	<div class="col m8 center-align offset-m2">
		<h3>Tasks</h3>
		<div class="row">
			<p class="card-panel z-depth-1 col m1" v-repeat="task: tasks | orderBy ''"> @{{ task}} </p>
		</div>
	</div>
	<br><br><br>
	<div class="input-field valign-wrapper col m4 offset-m4">
		<input id="newtask" type="text" placeholder="I need to..." v-model="newTask" v-on="keyup:addTask | key 'enter'"></input>
		<label for="newtask">Enter new Task</label> <br>
	</div>
</div>