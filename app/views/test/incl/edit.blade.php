<div id="edit" class="modal modal-fixed-footer bottom-sheet"> <!-- v-model="lokasi" -->
    <form method="POST" v-on="submit: onUpdateForm(lokasi, $event)">
	    <div class="modal-content row">
	    	<div class="col m4">
				<div class="input-field">
	                <i class="mdi-navigation-check prefix"></i>
	                <label for="noid" class="active">ID</label>
	                <input name="noid" type="text" id="noid" v-model="lokasi.noid" v-attr="disabled: true">
	            </div>
	            <div class="input-field">
	                <i class="mdi-social-location-city prefix"></i>
	                <label for="prv" class="active">Provinsi</label>
	                <input name="prv" type="text" id="prv" v-model="lokasi.prv">
	            </div>
	    	</div>
	    	<div class="col m4">
				<div class="input-field">
	                <i class="mdi-maps-directions prefix"></i>
	                <label for="alamat" class="active">Alamat</label>
	                <input name="alamat" type="text" id="alamat" v-model="lokasi.alamat">
	            </div>
		    	<div class="input-field">
	                <i class="mdi-maps-place prefix"></i>
	                <label for="lat" class="active">Koordinat Latitude</label>
	                <input name="lat" type="text" id="lat" v-model="lokasi.lat">
	            </div>
            </div>
	    	<div class="col m4">
	    		<div class="input-field">
	                <i class="mdi-social-domain prefix"></i>
	                <label for="kbp" class="active">Kabupaten</label>
	                <input name="kbp" type="text" id="kbp" v-model="lokasi.kbp">
	            </div>
	            <div class="input-field">
	                <i class="mdi-maps-place prefix"></i>
	                <label for="lon" class="active">Koordinat Longitude</label>
	                <input name="lon" type="text" id="lon" v-model="lokasi.lon">
	            </div>
            </div>
	    </div>
	    <div class="modal-footer">
	      	<a href="#!" class=" modal-action modal-close waves-effect waves-green btn">Batal</a>
	      	<button class="btn waves-effect waves-light" type="submit" name="action">Ubah Data
	                <i class="mdi-content-send right"></i>
	        </button>
	    </div>
    </form>
</div>