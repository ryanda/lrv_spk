<!DOCTYPE html>
<html>
<head>
	@include('test.incl.head')
</head>
<body>
	<main style="margin-top: 20px ">
		<div class="container">
			<div class="row">

				{{-- ajax app --}}
				<div id="rest">
					@include('test.incl.all')
					@include('test.incl.add')
					@include('test.incl.edit')
					@include('test.incl.delete')
					@include('test.incl.detail')
				</div>

				{{-- simple todos vuejs --}}
				@include('test.incl.todos')
				{{-- end simple todos --}}

			</div>
		</div>
	</main>
	
	{{-- {{HTML::script('js/reloadr.js')}} --}}
	{{HTML::script('js/vue.js')}}
	{{HTML::script('js/jquery.min.js')}}
	{{HTML::script('js/vue-resource.min.js')}}
	{{HTML::script('js/app.js')}}
	{{HTML::script('js/materialize.min.js')}}
	<script>
		$(document).ready( function() {
			$('.modal-trigger').leanModal({
				dismissible: false,
			});
			$('.modal-footer a').css('margin', '5px 5px');
			$('tbody tr td a').css('padding', '0 1rem');
		});
		
	</script>

	@show
</body>
</html>