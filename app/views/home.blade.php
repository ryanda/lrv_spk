@extends('default')
@section('body')
<span class="center-align">
	<h3>Selamat Datang {{ Auth::user()->name }} <br>di Sistem Penunjang Keputusan</h3>
	<p>Selamat Datang di Sistem Pendukung Keputusan Penentuan Pembangunan SPBU Baru. <br> Sistem ini digunakan untuk membantu menyeleksi tempat untuk pembangunan SPBU baru.</p>
	<h4><strong>Anda Login sebagai {{ Auth::user()->roles[0]->name }}</strong></h4>
</span>
@stop
@section('js')
$('.collection a:nth-child(1)').addClass('active');
@stop
