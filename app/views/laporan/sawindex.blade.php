@extends('default')
@section('body')
<h1>Data Laporan</h1>
<table class="bordered striped ">
    <thead>
        <tr>
            <th data-field="no">No</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Lokasi">Lokasi</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas">Krt 1</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan">Krt 2</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan">Krt 3</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri">Krt 4</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk">Krt 5</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat">Krt 6</th>
            <th>Nilai</th>
            <th>Detail</th>
        </tr>
    </thead>

    <tbody>
    @foreach($hasil as $data)
        <tr>
            <td>{{$no}}</td> <?php $no++ ?>
            <td>{{$data->lokasi->alamat}}</td>
            <td>{{$data->k1}}</td>
            <td>{{$data->k2}}</td>
            <td>{{$data->k3}}</td>
            <td>{{$data->k4}}</td>
            <td>{{$data->k5}}</td>
            <td>{{$data->k6}}</td>
            <td><strong>{{$data->nilai}}</strong></td>
            <td>
                <a href="{{ route('laporandetail', $data->id) }}" class="waves-effect waves-light btn"><i class="mdi-content-send"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="fixed-action-btn tooltipped" style="bottom: 45px; right: 24px;" data-position="left" data-delay="30" data-tooltip="Hitung!">
    <a href="{{ route('hitungsaw') }}" class="btn-floating btn-large red">
        <i class="large mdi-file-cloud"></i>
    </a>
</div>
@stop
@section('js')
$('.collection a:nth-child(8)').addClass('active');
@stop