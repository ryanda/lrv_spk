@extends('default')
@section('body')

	<h3 class="center-align">Detail Laporan</h3>
	<div class="row">
        <div class="col s12">
          <div class="card blue-grey darken-1 center-align">
            <div class="card-content white-text">
				<span class="card-title">Data {{ $lokasi->alamat}}</span>
				<p><i class="mdi-editor-format-list-numbered"></i> No :<strong>{{ $lokasi->id}}</strong></p>
				<p><i class="mdi-navigation-check"></i> ID : <strong>{{ $lokasi->noid}}</strong></p>
				<p><i class="mdi-maps-directions"></i> Alamat :  <strong>{{ $lokasi->alamat}}</strong></p>
				<p><i class="mdi-social-domain"></i> Kabupaten : <strong>{{ $lokasi->kbp}}</strong></p>
				<p><i class="mdi-social-location-city"></i> Provinsi : <strong>{{ $lokasi->prv}}</strong></p>
				<p><i class="mdi-maps-place"></i> Posisi : <strong><span id="lat">{{ $lokasi->lat}}</span>, <span id="lon">{{ $lokasi->lon}}</span></strong></p>
            </div>
            <div class="card-action">
				<a href="#">menuju laporan saw</a>
				<a href='#'>menuju laporan ahp</a>
            </div>
          </div>
        </div>
      </div>

    <div class="row">
		<div class="col s2">
			<p class="flow-text">Nilai 1 <br> {{$hasil->k1}}</p>
		</div>
		<div class="col s2">
			<p class="flow-text">Nilai 2 <br> {{$hasil->k2}}</p>
		</div>
		<div class="col s2">
			<p class="flow-text">Nilai 3 <br> {{$hasil->k3}}</p>
		</div>
		<div class="col s2">
			<p class="flow-text">Nilai 4 <br> {{$hasil->k4}}</p>
		</div>
		<div class="col s2">
			<p class="flow-text">Nilai 5 <br> {{$hasil->k5}}</p>
		</div>
		<div class="col s2">
			<p class="flow-text">Nilai 6 <br> {{$hasil->k6}}</p>
		</div>
    </div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	    <a class="btn-floating btn-large red">
	      <i class="mdi-navigation-apps left"></i>
	    </a>
    <ul>
      <li><a href="{{ route('nilaidetail', $nilai->id) }}" class="btn-floating purple darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Nilai Data"><i class="mdi-content-reply"></i></a></li> <!--back-->
      <li><a href="#modal" class="modal-trigger btn-floating red darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Hapus!"><i class="mdi-action-delete"></i></a></li> <!--delete-->      
    </ul>
  </div>

  <div id="modal" class="modal">
    <div class="modal-content">
      <h4>Anda yakin mau menghapus data ini ?</h4>
	<p><i class="mdi-navigation-check left"></i>ID : <strong>{{ $lokasi->noid}}</strong></p>
	<p><i class="mdi-maps-directions left"></i>Alamat :  <strong>{{ $lokasi->alamat}}</strong></p>
	<p><i class="mdi-social-domain left"></i>Kabupaten : <strong>{{ $lokasi->kbp}}</strong></p>
	<p><i class="mdi-social-location-city left"></i>Provinsi : <strong>{{ $lokasi->prv}}</strong></p>
	<p><i class="mdi-maps-place left"></i>Posisi : <strong>{{ $lokasi->lat}}, {{ $lokasi->lon}}</strong></p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat right">Tutup</a>
      <a href="{{ route('laporanhapus', $hasil->id) }}" class=" modal-action modal-close waves-effect waves-green btn red left">Hapus</a>
    </div>
  </div>
@stop
@section('js')
$('.collection a:nth-child(7)').addClass('active');
$('.collection a:nth-child(8)').addClass('active');
$('.modal-trigger').leanModal();
@stop