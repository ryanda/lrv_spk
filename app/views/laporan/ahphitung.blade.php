@extends('default')
@section('body')

{{ Form::model($nilai, ['route' => 'hitungpostahp', 'method' => 'POST']) }}
<div class="center-align">
    <br><br>
    <h5> Anda yakin ingin memperbarui perhitungan? </h5>
    <br><br>
    <button class="btn waves-effect waves-light" type="submit" name="action">Hitung!
    <i class="mdi-content-send right"></i>
  </button>
</div>

<div style="display:none">

<div class="row" >
@foreach ($nilai as $data) 
    <div class="col s2">
      <div class="card-panel"> 
        {{ Form::text('k1[]', $h1 = $data->krt1 * $krt[0])}}
        {{ Form::text('k2[]', $h2 = $data->krt2 * $krt[1])}}
        {{ Form::text('k3[]', $h3 = $data->krt3 * $krt[2])}}
        {{ Form::text('k4[]', $h4 = $data->krt4 * $krt[3])}}
        {{ Form::text('k5[]', $h5 = $data->krt5 * $krt[4])}}
        {{ Form::text('k6[]', $h6 = $data->krt6 * $krt[5])}}
        {{ Form::text('jumlah[]', $h1+$h2+$h3+$h4+$h5+$h6)}}
        </div>
    </div>
@endforeach
</div>


<div class="row">
@foreach ($nilai as $data) 
    <div class="col s2">
      <div class="card-panel"> 
        {{ Form::text('n1[]', $n1 = $data->krt1)}}
        {{ Form::text('n2[]', $n2 = $data->krt2)}}
        {{ Form::text('n3[]', $n3 = $data->krt3)}}
        {{ Form::text('n4[]', $n4 = $data->krt4)}}
        {{ Form::text('n5[]', $n5 = $data->krt5)}}
        {{ Form::text('n6[]', $n6 = $data->krt6)}}
        {{ Form::text('jn[]', $n1+$n2+$n3+$n4+$n5+$n6)}}
        </div>
    </div>
@endforeach
</div>
</div>
{{ Form::close() }}

@stop
@section('js')
$('.collection a:nth-child(7)').addClass('active');
@stop