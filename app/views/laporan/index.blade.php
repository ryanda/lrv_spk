@extends('default')
@section('body')
<h3 class="center-align">Metode Perhitungan</h3>
<div class="row">
	<div class="col s6">
		<div class="card">
		    <div class="card-image waves-effect waves-block waves-light">
		    </div>
		    <div class="card-content">
		      <span class="card-title activator grey-text text-darken-4">Laporan AHP <i class="mdi-navigation-more-vert right"></i></span>
		      <p><a href="laporanahp">Laporan AHP</a></p>
		    </div>
		    <div class="card-reveal">
		      <span class="card-title grey-text text-darken-4">Laporan AHP <i class="mdi-navigation-close right"></i></span>
		      <h3>Laporan AHP</h3>
		    </div>
		</div>
	</div>

	<div class="col s6">
		<div class="card">
		    <div class="card-image waves-effect waves-block waves-light">
		    </div>
		    <div class="card-content">
		      <span class="card-title activator grey-text text-darken-4">Laporan SAW <i class="mdi-navigation-more-vert right"></i></span>
		      <p><a href="laporansaw">Laporan SAW</a></p>
		    </div>
		    <div class="card-reveal">
		      <span class="card-title grey-text text-darken-4">Laporan SAW <i class="mdi-navigation-close right"></i></span>
		      <h3>Laporan SAW</h3>
		    </div>
		</div>
	</div>
</div>


@stop
@section('js')
$('.collection a:nth-child(1)').addClass('active');
@stop