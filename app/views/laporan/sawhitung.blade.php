@extends('default')
@section('body')

{{ Form::model($nilai, ['route' => 'hitungpostsaw', 'method' => 'POST']) }}
<div class="center-align">
    <br><br>
    <h5> Anda yakin ingin memperbarui perhitungan? </h5>
    <br><br>
    <button class="btn waves-effect waves-light" type="submit" name="action">Hitung!
    <i class="mdi-content-send right"></i>
  </button>
</div>

<div style="display:none">

<div class="row">
@foreach ($nilai as $data) 
    <div class="col s2">
      <div class="card-panel"> 
        {{ Form::text('n1[]', $n1 = $data->krt1)}}
        {{ Form::text('n2[]', $n2 = $data->krt2)}}
        {{ Form::text('n3[]', $n3 = $data->krt3)}}
        {{ Form::text('n4[]', $n4 = $data->krt4)}}
        {{ Form::text('n5[]', $n5 = $data->krt5)}}
        {{ Form::text('n6[]', $n6 = $data->krt6)}}
        {{ Form::text('jn[]', $n1+$n2+$n3+$n4+$n5+$n6)}}
        </div>
    </div>
@endforeach
</div>

<div class="row" >
@foreach ($nilai as $data) 

    <?php
    $j1 = $data->krt1/ $krt1;
    $j2 = $data->krt2/ $krt2;
    $j3 = $data->krt3/ $krt3;
    $j4 = $data->krt4/ $krt4;
    $j5 = $data->krt5/ $krt5;
    $j6 = $data->krt6/ $krt6;
    ?>

    <div class="col s2">
      <div class="card-panel"> 
        {{ Form::text('k1[]', $h1 = round($j1 * $krt[0], 3) )}}
        {{ Form::text('k2[]', $h2 = round($j2 * $krt[1], 3) )}}
        {{ Form::text('k3[]', $h3 = round($j3 * $krt[2], 3) )}}
        {{ Form::text('k4[]', $h4 = round($j4 * $krt[3], 3) )}}
        {{ Form::text('k5[]', $h5 = round($j5 * $krt[4], 3) )}}
        {{ Form::text('k6[]', $h6 = round($j6 * $krt[5], 3) )}}
        {{ Form::text('jumlah[]', $h1+$h2+$h3+$h4+$h5+$h6)}}
        </div>
    </div>
@endforeach
</div>


</div>
{{ Form::close() }}

@stop
@section('js')
$('.collection a:nth-child(8)').addClass('active');
@stop