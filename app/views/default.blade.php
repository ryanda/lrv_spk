<!DOCTYPE html>
<html>
<head>
    <title>Sistem Penunjang Keputusan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{HTML::style('css/materialize.min.css')}}
    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }
        main {
            flex: 1 0 auto;
        }
        .container{width:90%}            
        #ubah tr td { width: 14%}  
        table tbody tr td .btn{
            padding: 0 1rem;
        }
    </style>
</head>
<body>
    @include('partials.header')

    <main style="margin-top: 20px">
        <div class="container">
          <div class="row">

            @if (Auth::check())
                <div class="col s3">
                    <div class="collection">
                        @include('partials.sidebar')
                    </div>
                </div>
                <div class="col s9">
                    @yield('body')
                </div>
            @else 
                <div class="col s4 offset-s4">
                    @yield('body')
                </div>
            @endif

          </div>
        </div>
    </main>
	
    @include('partials.footer')

{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/materialize.min.js')}}
<script>
$(document).ready( function() {
    $('select').material_select();
    $('.tooltipped').tooltip({delay: 50});

    @if (Session::has('pesan'))
        Materialize.toast('{{ Session::get('pesan') }}', 4000);
    @endif

    @yield('js')
});
</script>
@show
</body>
</html>