<?php
$data = Lokasi::count();
$nilai = NilaiData::count();
$hasilahp = HasilAhp::count();
$hasilsaw = HasilSaw::count();
?>

    <a href="/" class="collection-item"><i class="mdi-action-home left"></i>Home</a>

@if (Auth::user()->hasRole('Manager'))
    <a class="collection-item"><i class="mdi-communication-location-on left"></i>Data Lokasi<i class="mdi-content-clear right"></i></a>
@else
    <a href="/lokasi" class="collection-item"><i class="mdi-communication-location-on left"></i>Data Lokasi<span class="data badge">{{$data}}</span></a>
@endif
    <a href="/nilai" class="collection-item"><i class="mdi-toggle-check-box left"></i>Data Nilai<span class="data badge">{{$nilai}}</span></a>

@if (Auth::user()->hasRole('User'))
    <a class="collection-item"><i class="mdi-communication-comment left"></i>Kriteria AHP<i class="mdi-content-clear right"></i></a>
    <a class="collection-item"><i class="mdi-communication-comment left"></i>Kriteria SAW<i class="mdi-content-clear right"></i></a>
@else
    <a href="/ahp" class="collection-item"><i class="mdi-communication-comment left"></i>Kriteria AHP</a>
    <a href="/saw" class="collection-item"><i class="mdi-communication-comment left"></i>Kriteria SAW</a>
@endif

@if (Auth::user()->hasRole('User'))
    <a class="collection-item"><i class="mdi-action-account-circle left"></i>Data Pengguna<i class="mdi-content-clear right"></i></a>
@else
    <a href="/users" class="collection-item"><i class="mdi-action-account-circle left"></i>Data Pengguna</a>
@endif

@if (Auth::user()->hasRole('Manager'))
    <a href="/laporanahp" class="collection-item"><i class="mdi-device-dvr left"></i>Laporan AHP<span class="data badge">{{$hasilahp}}</span></a>
    <a href="/laporansaw" class="collection-item"><i class="mdi-device-dvr left"></i>Laporan SAW<span class="data badge">{{$hasilsaw}}</span></a>
@else
    <a class="collection-item"><i class="mdi-action-account-circle left"></i>Laporan AHP<i class="mdi-content-clear right"></i></a>
    <a class="collection-item"><i class="mdi-action-account-circle left"></i>Laporan AHP<i class="mdi-content-clear right"></i></a>
@endif
    
	<a href="/rest" class="collection-item"><i class="mdi-content-add-circle left"></i>AJAX Lokasi<span class="new badge">AJAX</span></a>
	<a href="/api" class="collection-item"><i class="mdi-content-add-circle-outline left"></i>API Lokasi<span class="new badge">JSON</span></a>
