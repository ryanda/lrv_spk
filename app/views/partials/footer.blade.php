@if (Auth::check())
    @if (Auth::user()->hasRole("Admin"))    <footer class="page-footer cyan lighten-2">    @endif
    @if (Auth::user()->hasRole("Manager"))  <footer class="page-footer blue lighten-2">    @endif
    @if (Auth::user()->hasRole("User"))     <footer class="page-footer orange lighten-2">  @endif
@else
    <footer class="page-footer green lighten-2">
@endif


      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">Lorem ipsum Reprehenderit reprehenderit in esse est amet aliquip cillum culpa reprehenderit ut ea Duis dolore nisi consectetur magna magna deserunt dolor adipisicing ut dolore deserunt Ut adipisicing pariatur eu.
              @if (!Auth::check())
                <span><br>default login: <strong> ryand, sobri, piqri, mizwar</strong> pass: <strong>123456</strong></span>
              @endif
            </p>
        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text"><i class="mdi-action-account-circle left"></i>Anggota</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-add-circle left"></i>Muhammad Ryanda Putra</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-mail left"></i>Sobri</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-send left"></i>Piqiw</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-archive left"></i>Mizwar</a></li>
          </ul>
      </div>
    </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
        © 2015 Copyright ryandaputra
        </div>
    </div>
    </footer>