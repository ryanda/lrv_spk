<header>

@if (Auth::check())
    @if (Auth::user()->hasRole("Admin"))    <nav class="cyan lighten-2">    @endif
    @if (Auth::user()->hasRole("Manager"))  <nav class="blue lighten-2">    @endif
    @if (Auth::user()->hasRole("User"))     <nav class="orange lighten-2">  @endif
@else
    <nav class="green lighten-2">
@endif

        <div class="nav-wrapper container">
          <a href="{{route('home')}}" class="brand-logo">Sistem Penunjang Keputusan AHP</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            @if (Auth::check())
                <li><a><i class="mdi-action-account-circle left"></i>{{ Auth::user()->name }}</a></li>
                <li><a><i class="mdi-av-my-library-books left"></i>{{ Auth::user()->roles[0]->name }}</a></li>
                <li><a href="{{route('logout')}}"><i class="mdi-action-shop left"></i>Keluar</a></li>
            @else
                <li><a href="/login"><i class="mdi-hardware-security left"></i>Login</a></li>
            @endif
          </ul>
        </div>
    </nav>

</header>