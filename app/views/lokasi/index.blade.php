@extends('default')
@section('body')
<h3 class="center-align">Data Lokasi</h3>
<table class="bordered striped">
    <thead>
        <tr>
            <th>No</th>
            <th>ID</th>
            <th>Alamat</th>
            <th>Kabupaten</th>
            <th>Provinsi</th>
            <th>Detail</th>
        </tr>
    </thead>

    <tbody>
    @foreach ($lokasi as $data) 
        <tr>
            <td>{{$no}}</td> <?php $no++ ?>
            <td>{{$data->noid}}</td>
            <td>{{$data->alamat}}</td>
            <td>{{$data->kbp}}</td>
            <td>{{$data->prv}}</td>
            <td>
            <a href="{{ route('lokasidetail', $data->id) }}" class="waves-effect waves-light btn"><i class="mdi-content-send"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="fixed-action-btn tooltipped" style="bottom: 45px; right: 24px;" data-position="left" data-delay="30" data-tooltip="Tambah Data">
    <a href="{{ route('lokasitambah') }}" class="btn-floating btn-large red">
        <i class="large mdi-content-add"></i>
    </a>
</div>
@stop
@section('js')
$('.collection a:nth-child(2)').addClass('active');
@stop