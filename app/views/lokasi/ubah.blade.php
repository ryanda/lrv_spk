@extends('default')
@section('body')
    <h3 class="center-align">Edit Lokasi</h3>
        
    {{ Form::model($lokasi, ['route' => ['editlokasi', $lokasi->id], 'method' => 'PUT']) }}
    <div class="input-field">
        <i class="mdi-navigation-check prefix"></i>
        {{Form::label('noid','No ID', ['class' => 'active'])}}
        {{Form::text('noid', $lokasi->noid)}}
    </div>
    <div class="input-field">
        <i class="mdi-maps-directions prefix"></i>
        {{Form::label('alamat','Alamat', ['class' => 'active'])}}
        {{Form::text('alamat', $lokasi->alamat)}}
    </div>
    <div class="input-field">
        <i class="mdi-social-domain prefix"></i>
        {{Form::label('kbp','Kabupaten', ['class' => 'active'])}}
        {{Form::text('kbp', $lokasi->kbp)}}
    </div>
    <div class="input-field">
        <i class="mdi-social-location-city prefix"></i>
        {{Form::label('prv','Provinsi', ['class' => 'active'])}}
        {{Form::text('prv', $lokasi->prv)}}
    </div>
    <div class="input-field">
        <i class="mdi-maps-place prefix"></i>
        {{Form::label('lat','Koordinat Latitude', ['class' => 'active'])}}
        {{Form::text('lat', $lokasi->lat)}}
    </div>
    <div class="input-field">
        <i class="mdi-maps-place prefix"></i>
        {{Form::label('lon','Koordinat Longitude', ['class' => 'active'])}}
        {{Form::text('lon', $lokasi->lon)}}
    </div>
   <br><br>
   <button class="btn waves-effect waves-light" type="submit" name="action">Submit
    <i class="mdi-content-send right"></i>
  </button>
    {{ Form::close() }}
@stop
@section('js')
$('.collection a:nth-child(2)').addClass('active');
@stop