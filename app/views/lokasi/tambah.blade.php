@extends('default')
@section('body')
    <div class="row">
        <div class="col s6 offset-s3">
            <h2>Masukkan Data Lokasi</h2>
            {{ Form::open(['route' => 'tambahlokasi', 'method' => 'post']) }}
            <div class="input-field">
                <i class="mdi-maps-directions prefix"></i>
                {{Form::label('alamat','Alamat', ['class' => 'active'])}}
                {{Form::text('alamat', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-social-domain prefix"></i>
                {{Form::label('kbp','Kabupaten', ['class' => 'active'])}}
                {{Form::text('kbp', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-social-location-city prefix"></i>
                {{Form::label('prv','Provinsi', ['class' => 'active'])}}
                {{Form::text('prv', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-maps-place prefix"></i>
                {{Form::label('lat','Koordinat Latitude', ['class' => 'active'])}}
                {{Form::text('lat', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-maps-place prefix"></i>
                {{Form::label('lon','Koordinat Longitude', ['class' => 'active'])}}
                {{Form::text('lon', null)}}
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="mdi-content-send right"></i>
            </button>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('js')
$('.collection a:nth-child(2)').addClass('active');
$('.modal-trigger').leanModal();
@stop