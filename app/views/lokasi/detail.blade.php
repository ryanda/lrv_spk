@extends('default')
@section('body')

	<h3 class="center-align">Data Lokasi</h3>
	<div class="row center-align">
		<div class="col m12">
			<p><i class="mdi-editor-format-list-numbered "></i>No :<strong>{{ $lokasi->id}}</strong></p>
			<p><i class="mdi-navigation-check "></i>ID : <strong>{{ $lokasi->noid}}</strong></p>
			<p><i class="mdi-maps-directions "></i>Alamat :  <strong>{{ $lokasi->alamat}}</strong></p>
		</div>
		<div class="col m6">
	<p><i class="mdi-social-domain "></i>Kabupaten : <strong>{{ $lokasi->kbp}}</strong></p>
		</div>
		<div class="col m6">
	<p><i class="mdi-social-location-city "></i>Provinsi : <strong>{{ $lokasi->prv}}</strong></p>
		</div>
		<div class="col m12">
	<p><i class="mdi-maps-place "></i>Posisi : <strong><span id="lat">{{ $lokasi->lat}}</span>, <span id="lon">{{ $lokasi->lon}}</span></strong></p>
		</div>
	</div>
		
	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	    <a class="btn-floating btn-large red">
	      <i class="mdi-navigation-apps left"></i>
	    </a>
	    <ul>
	      <li><a href="{{ route('lokasi') }}" class="btn-floating purple darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Data Lokasi"><i class="mdi-content-reply"></i></a></li> <!--back-->
	      <li><a href="{{ route('nilaidetail', $lokasi->id) }}" class="btn-floating blue darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Data Nilai"><i class="mdi-content-forward"></i></a></li> <!--next-->
	      <li><a href="{{ route('lokasiedit', $lokasi->id) }}" class="btn-floating yellow darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Edit Data"><i class="mdi-content-create"></i></a></li> <!--edit-->
	      <li><a href="#modal" class="modal-trigger btn-floating red darken-3 tooltipped" data-position="left" data-delay="30" data-tooltip="Hapus!"><i class="mdi-action-delete"></i></a></li> <!--delete-->
	    </ul>
	</div>

<div id="modal" class="modal">
    <div class="modal-content">
      <h4>Anda yakin mau menghapus data ini ?</h4>
	<p><i class="mdi-navigation-check left"></i>ID : <strong>{{ $lokasi->noid}}</strong></p>
	<p><i class="mdi-maps-directions left"></i>Alamat :  <strong>{{ $lokasi->alamat}}</strong></p>
	<p><i class="mdi-social-domain left"></i>Kabupaten : <strong>{{ $lokasi->kbp}}</strong></p>
	<p><i class="mdi-social-location-city left"></i>Provinsi : <strong>{{ $lokasi->prv}}</strong></p>
	<p><i class="mdi-maps-place left"></i>Posisi : <strong>{{ $lokasi->lat}}, {{ $lokasi->lon}}</strong></p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat right">Tutup</a>
      <a href="{{ route('lokasihapus', $lokasi->id) }}" class=" modal-action modal-close waves-effect waves-green btn red left">Hapus</a>
    </div>
  </div>
@stop
@section('js')
$('.collection a:nth-child(2)').addClass('active');
$('.modal-trigger').leanModal();
@stop