@extends('default')
@section('body')
<div class="card-panel red white-text darken-3 center-align z-depth-2">
	<h3>Server Error <br> 403 Access Denied</h3>
	<p>Maaf, anda tidak memiliki akses terhadap data.</p>
</div>
@stop