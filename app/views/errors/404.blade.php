@extends('default')
@section('body')
<div class="card-panel red white-text darken-3 center-align z-depth-2">
	<h3>Server Error <br> 404 Not Found</h3>
	<p>Maaf, data tidak ditemukan.</p>
</div>
@stop