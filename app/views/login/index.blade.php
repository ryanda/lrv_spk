@extends('default')
@section('body')
	<div class="section"> 
		{{ Form::open(array('url' => '/', 'method' => 'post')) }}
		<div class="input-field">
			<i class="mdi-action-account-circle prefix"></i>
			{{Form::label('username','Username', ['class' => 'active'])}}
			{{Form::text('username', null,array('class' => 'validate', 'placeholder' => 'masukkan username anda'))}}
		</div>
		<div class="input-field">
			<i class="mdi-action-https prefix"></i>
			{{Form::label('password','Password', ['class' => 'active'])}}
			{{Form::password('password',array('class' => 'validate', 'placeholder' => 'masukkan password anda'))}}
		</div>
	    <button class="btn waves-effect waves-light" type="submit" name="action">Login
	        <i class="mdi-content-send right"></i>
	    </button>
		{{ Form::close() }}
	</div>
@stop{{-- 
@section('js')

@if ($errors->all())
	@foreach ($errors->all() as $message)
		Materialize.toast('{{$message}}', 4000);
	@endforeach
@endif
@if (Session::has('pesan'))
	Materialize.toast('{{ Session::get('pesan') }}', 4000);
@endif

@stop --}}