@extends('default')
@section('body')
<h3 class="center-align">Daftar Admin</h3>
<div class="row">
@foreach ($user as $data)
	<div class="col s6">
		<div class="card">
		    <div class="card-image waves-effect waves-block waves-light">
		      {{HTML::image('img/'.$data->file, null, ['class' => 'activator responsive-img'])}}
		    </div>
		    <div class="card-content">
		      <span class="card-title activator grey-text text-darken-4">{{$data->name}} <i class="mdi-navigation-more-vert right"></i></span>
		      <p><a href="#">{{'@'.$data->username}}</a></p>
		    </div>

		    @if ($data->hasRole('Admin'))
			    <div class="card-reveal cyan white-text">
			@elseif ($data->hasRole('Manager'))
			    <div class="card-reveal blue white-text">
			@elseif ($data->hasRole('User'))
			    <div class="card-reveal orange white-text">
			@endif
				
		      <span class="card-title"><br>{{$data->name}} as {{$data->roles[0]->name}}
		       <i class="mdi-navigation-close right"></i></span>
		      <h3> {{$data->roles[0]->name}} </h3>
		    </div>
		</div>
	</div>
@endforeach
</div>

@if (Auth::user()->hasRole("Admin"))
<div class="fixed-action-btn tooltipped" style="bottom: 45px; right: 24px;" data-position="left" data-delay="30" data-tooltip="Tambah Pengguna">
    <a href="{{ route('addusers') }}" class="btn-floating btn-large red">
        <i class="large mdi-content-add"></i>
    </a>
</div>
@endif

@stop
@section('js')
$('.collection a:nth-child(6)').addClass('active');
@stop