@extends('default')
@section('body')
    <div class="row">
        <div class="col s6 offset-s3">
            <h3 class="center-align">Register Pengguna</h3>
            {{ HTML::ul($errors->all()) }}
            {{ Form::open(['route' => ['postusers'], 'method' => 'post', 'files' => true]) }}
            <div class="input-field">
                <i class="mdi-social-person prefix"></i>
                {{Form::label('name','Name', ['class' => 'active'])}}
                {{Form::text('name', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-action-account-circle prefix"></i>
                {{Form::label('username','Username', ['class' => 'active'])}}
                {{Form::text('username', null)}}
            </div>
            <div class="input-field">
                <i class="mdi-action-https prefix"></i>
                {{Form::label('password','Password', ['class' => 'active'])}}
                {{Form::password('password')}}
            </div>
            <div class="input-field">
                <i class="mdi-action-https prefix"></i>
                {{Form::label('password_confirmation','Password Konfirmasi', ['class' => 'active'])}}
                {{Form::password('password_confirmation')}}
            </div>
            <div class="input-field file-field">
                {{Form::input('text', null, null, ['class' => 'file-path validate'])}}
                <div class="btn">
                    <span>File</span>
                    {{ Form::file('file')}}
                </div>
            </div>
            
                <p>
                    {{ Form::radio('access', 'Admin', null, ['id' => 'adm']) }}
                    {{ Form::label('adm', 'Admin')}}
                </p>
                <p>
                    {{ Form::radio('access', 'Manager', null, ['id' => 'mng']) }}
                    {{ Form::label('mng', 'Manager')}}
                </p>
                <p>
                    {{ Form::radio('access', 'User', null, ['id' => 'usr']) }}
                    {{ Form::label('usr', 'User')}}
                </p>

            <button class="btn waves-effect waves-light" type="submit" name="action">Tambah
                <i class="mdi-content-send right"></i>
            </button>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('js')
$('.collection a:nth-child(6)').addClass('active');
@stop