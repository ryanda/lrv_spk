<!DOCTYPE html>
<html>
<head>
    <title>Sistem Penunjang Keputusan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{HTML::style('css/materialize.min.css')}}
    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }
        main {
            flex: 1 0 auto;
        }
        .container{width:90%}              
    </style>
</head>
<body>
    <header>
        <nav class="green lighten-2">
            <div class="nav-wrapper container">
              <a href="{{route('home')}}" class="brand-logo">Sistem Penunjang Keputusan</a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li class="active"><a><i class="mdi-social-person left"></i>as ADMIN</a>
                    </li>
                    <li><a><i class="mdi-content-reply-all left"></i>Logout</a>
                    </li>
              </ul>
            </div>
        </nav>
    </header>

    <main style="margin-top: 20px">
        <div class="container">
              @yield('body')
        </div>
    </main>
	
    <footer class="page-footer green lighten-2">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">Lorem ipsum Reprehenderit reprehenderit in esse est amet aliquip cillum culpa reprehenderit ut ea Duis dolore nisi consectetur magna magna deserunt dolor adipisicing ut dolore deserunt Ut adipisicing pariatur eu.</p>
        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text"><i class="mdi-action-account-circle left"></i>Anggota</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-add-circle left"></i>Muhammad Ryanda Putra</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-mail left"></i>Sobri</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-send left"></i>Piqiw</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-archive left"></i>Mizwar</a></li>
          </ul>
      </div>
    </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
        © 2015 Copyright ryandaputra
        </div>
    </div>
    </footer>

{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/materialize.min.js')}}
<script>
$(document).ready( function() {
     $('select').material_select();
     $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 30 // Creates a dropdown of 15 years to control year
      }); 
     $('.tooltipped').tooltip({delay: 50});
});
@yield('js')

</script>
@show
</body>
</html>