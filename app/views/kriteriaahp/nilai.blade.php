<div class="section">
    <h5>Matrik Nilai Kriteria</h5>
    <table class="bordered striped centered">
        <tr>
            <td><strong><em>Kriteria</em></strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Krt 1</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Krt 2</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Krt 3</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Krt 4</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Krt 5</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Krt 6</strong></td>
            <td><strong><em>Jumlah</em></strong></td>
            <td><strong><em>Prioritas</em></strong></td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Krt 1</strong></td>
            @foreach ($nilai[0] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Krt 2</strong></td>
            @foreach ($nilai[1] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Krt 3</strong></td>
            @foreach ($nilai[2] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Krt 4</strong></td>
            @foreach ($nilai[3] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Krt 5</strong></td>
            @foreach ($nilai[4] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Krt 6</strong></td>
            @foreach ($nilai[5] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
    </table>
</div>