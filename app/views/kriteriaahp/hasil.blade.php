<div class="section">
    <h5>Hasil Perhitungan</h5>
    <table class="bordered striped centered">
        <tr>
            <td><strong><em>Keterangan</em></strong></td>
            <td><strong><em>Nilai</em></strong></td>
        </tr>
        <tr>
            <td><strong>Jumlah</strong></td>
            <td>{{ $j = DB::table('rso_kons')->sum('hsl')}}</td>
        </tr>
        <tr>
            <td><strong>n(Jumlah kriteria)</strong></td>
            <td>{{ $n = DB::table('krt_banding')->count()}}</td>
        </tr>
        <tr>
            <td><strong>Maks(Jumlah/n)</strong></td>
            <td>{{ $m = $j / $n}}</td>
        </tr>
        <tr>
            <td><strong>CI((Maks-n)/n)</strong></td>
            <td>{{ $ci = ($m-$n)/$n}}</td>
        </tr>
        <tr>
            <td><strong>IR</strong></td>
            <td>{{ 1.24 }}</td>
        </tr>
        <tr>
            <td><strong>CR(CI/IR)</strong></td>
            <td>{{ $ci / 1.24}}</td>
        </tr>
    </table>
</div>