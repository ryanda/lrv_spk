<div class="section">
    <h5>Matrik Perbandingan Berpasangan</h5>  <!-- jumlah : atas - bawah -->
    <table class="bordered striped centered" id="ubah">
        <tr>
            <td><strong><em>Kriteria</em></strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Krt 1</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Krt 2</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Krt 3</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Krt 4</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Krt 5</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Krt 6</strong></td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Krt 1</strong></td>
            @foreach ($banding[0] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Krt 2</strong></td>
            @foreach ($banding[1] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Krt 3</strong></td>
            @foreach ($banding[2] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Krt 4</strong></td>
            @foreach ($banding[3] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Krt 5</strong></td>
            @foreach ($banding[4] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Krt 6</strong></td>
            @foreach ($banding[5] as $data)
            <td>{{$data}}</td>
            @endforeach
        </tr>
        <tr>    
            <td><strong><em>Jumlah</em></strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k1')}}</strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k2')}}</strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k3')}}</strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k4')}}</strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k5')}}</strong></td>
            <td><strong>{{ DB::table('krt_banding')->sum('k6')}}</strong></td>
        </tr>
    </table>
</div>