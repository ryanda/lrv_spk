@extends('default')
@section('body')
<h3 class="center-align">Kriteria Utama</h3>
@include('kriteriaahp.banding')
@include('kriteriaahp.nilai')
@include('kriteriaahp.jumlah')
@include('kriteriaahp.rasio')
@include('kriteriaahp.hasil')
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a href="{{ route('ahpubah')}}" class="btn-floating btn-large red">
		<i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
@stop
@section('js')
$('.collection a:nth-child(4)').addClass('active');
@stop