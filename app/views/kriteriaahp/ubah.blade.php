@extends('default')
@section('body')
<h3 class="center-align">Ubah Kriteria</h3>
		{{ Form::model($data, ['route' => 'ahpsimpan', 'method' => 'PUT']) }}


        <table class="bordered striped centered" id="ubah">
        <tr>
            <td><strong><em>Kriteria</em></strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Kriteria 1</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Kriteria 2</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Kriteria 3</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Kriteria 4</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Kriteria 5</strong></td>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Kriteria 6</strong></td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas"><strong>Kriteria 1</strong></td>
            <td>1</td>
            <td>{{ Form::select('21', $list, $data[0]->k2 ) }}</td>
            <td>{{ Form::select('31', $list, $data[0]->k3 ) }}</td>
            <td>{{ Form::select('41', $list, $data[0]->k4 ) }}</td>
            <td>{{ Form::select('51', $list, $data[0]->k5 ) }}</td>
            <td>{{ Form::select('61', $list, $data[0]->k6 ) }}</td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan"><strong>Kriteria 2</strong></td>
            <td>x</td>
            <td>1</td>
            <td>{{ Form::select('32', $list, $data[1]->k3 ) }}</td>
            <td>{{ Form::select('42', $list, $data[1]->k4 ) }}</td>
            <td>{{ Form::select('52', $list, $data[1]->k5 ) }}</td>
            <td>{{ Form::select('62', $list, $data[1]->k6 ) }}</td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan"><strong>Kriteria 3</strong></td>
            <td>x</td>
            <td>x</td>
            <td>1</td>
            <td>{{ Form::select('43', $list, $data[2]->k4 ) }}</td>
            <td>{{ Form::select('53', $list, $data[2]->k5 ) }}</td>
            <td>{{ Form::select('63', $list, $data[2]->k6 ) }}</td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri"><strong>Kriteria 4</strong></td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>1</td>
            <td>{{ Form::select('54', $list, $data[3]->k5 ) }}</td>
            <td>{{ Form::select('64', $list, $data[3]->k6 ) }}</td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk"><strong>Kriteria 5</strong></td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>1</td>
            <td>{{ Form::select('65', $list, $data[4]->k6 ) }}</td>
        </tr>
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat"><strong>Kriteria 6</strong></td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>x</td>
            <td>1</td>
        </tr>
    </table>
           <br><br>
           <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="mdi-content-send right"></i>
          </button>
        {{ Form::close() }}


<h3 class="center-align">Keterangan</h3>

    <table class="centered  striped bordered">
        <thead>
            <th>Intensitas Kepentingan</th>
            <th>Keterangan</th>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Kedua elemen sama pentingnya</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Elemen yang satu sedikit lebih penting daripada elemen yang lainnya</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Elemen yang satu lebih penting daripada elemen lainnya</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Satu elemen jelas lebih mutlak penting daripada elemen lainnya</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Satu elemen mutlak penting dari pada elemen lainnya</td>
            </tr>
        </tbody>
    </table>
            {{-- {{Form::submit('Register', array('class' => 'waves-effect waves-light btn'))}} --}}

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a href="{{route('ahp')}}" class="btn-floating btn-large red">
		<i class="large mdi-content-reply"></i>
	</a>
</div>
@stop
@section('js')
$('.collection a:nth-child(4)').addClass('active');
@stop