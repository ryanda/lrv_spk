<!DOCTYPE html>
<html>
<head>
    <title>Sistem Penunjang Keputusan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    {{HTML::style('css/materialize.min.css')}}
    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }
        main {
            flex: 1 0 auto;
        }              
    </style>
</head>
<body>
    <header>
        <nav class="green lighten-2">
            <div class="nav-wrapper container">
              <a href="{{route('home')}}" class="brand-logo">Sistem Penunjang Keputusan</a>
            </div>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                @if (Auth::check())
                    <li><a class="tooltipped" data-position="bottom" data-tooltip="NIP : {{ Auth::user()->nip }}, Nama : {{ Auth::user()->nama }}, Username : {{ Auth::user()->username }}"><i class="mdi-action-android left"></i>{{ Auth::user()->nama }}</a></li>
                    <li><a href="{{route('logout')}}"><i class="mdi-action-shop left"></i>Keluar</a></li>
                @else
                    <li><a href="#"><i class="mdi-communication-vpn-key left"></i>Login</a></li>
                    <li><a href="#"><i class="mdi-action-account-circle left"></i>Sign Up</a></li>
                @endif
            </ul>
        </nav>
    </header>

    <main>
        <div class="container">
            @yield('body')
        </div>
    </main>
	
    <footer class="page-footer green lighten-2">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">Lorem ipsum Reprehenderit reprehenderit in esse est amet aliquip cillum culpa reprehenderit ut ea Duis dolore nisi consectetur magna magna deserunt dolor adipisicing ut dolore deserunt Ut adipisicing pariatur eu.</p>
        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text"><i class="mdi-action-account-circle left"></i>Links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-add-circle left"></i>Link 1</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-mail left"></i>Link 2</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-send left"></i>Link 3</a></li>
              <li><a class="grey-text text-lighten-3" href="#!"><i class="mdi-content-archive left"></i>Link 4</a></li>
          </ul>
      </div>
    </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
        © 2015 Copyright ryandaputra
        </div>
    </div>
    </footer>

{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/materialize.min.js')}}
<script>
$(document).ready( function() {
     $('select').material_select();
     $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 30 // Creates a dropdown of 15 years to control year
      }); 
     $('.tooltipped').tooltip({delay: 50});
});

</script>
@show
</body>
</html>