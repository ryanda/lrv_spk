@extends('default')
@section('body')
<h3 class="center-align">Kriteria</h3>

<div class="section">
    <table class="bordered striped centered">
        <tr>
            <td><strong>Kriteria</strong></td>
            <td><strong>Beban</strong></td>
        </tr>
        @foreach ($beban as $data)
        <tr>
            <td class="tooltipped" data-position="top" data-delay="30" data-tooltip="{{$data->krt}}">Kriteria {{$data->krt}}</td>
            <td>{{$data->beban}}</td>
            
        </tr>
        @endforeach
    </table>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a href="{{ route('sawubah')}}" class="btn-floating btn-large red">
		<i class="large mdi-editor-mode-edit"></i>
	</a>
</div>
@stop
@section('js')
$('.collection a:nth-child(5)').addClass('active');
@stop