@extends('default')
@section('body')
<h3 class="center-align">Ubah Kriteria</h3>
		{{ Form::model($beban, ['route' => 'sawsimpan', 'method' => 'PUT']) }}
           <button class="btn waves-effect waves-light" type="submit" name="action">Ubah
            <i class="mdi-content-send right"></i>
          </button>
           <br><br>
          
            <div class="row">
            @foreach($beban as $data)
                <div class="col s12 m6">
                    <div class="input-field">
                        {{ Form::label('bbn[]', $data->krt, ['class' => 'active']) }}
                        {{ Form::select('bbn[]', $list, $data->beban ) }}
                    </div>
                </div>
            @endforeach
            </div>

            {{-- {{Form::submit('Register', array('class' => 'waves-effect waves-light btn'))}} --}}
        {{ Form::close() }}

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a href="{{route('saw')}}" class="btn-floating btn-large red">
		<i class="large mdi-content-reply"></i>
	</a>
</div>
@stop
@section('js')
$('.collection a:nth-child(5)').addClass('active');
@stop