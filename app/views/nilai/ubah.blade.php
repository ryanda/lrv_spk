@extends('default')
@section('body')
    <h3 class="center-align">Edit Nilai</h3>
        <p class="flow-text"> {{$lokasi->noid}} </p>

    {{ Form::model($data, ['route' => ['editnilai', $data->id], 'method' => 'PUT']) }}
    <button class="btn waves-effect waves-light" type="submit" name="action">Ubah
    <i class="mdi-content-send right"></i>
    </button>
   <br><br>

    <div class="row">
        <div class="input-field col s6">
            {{ Form::label('krt1', '1. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt1', $list, $data->krt1) }}
        </div>
        <div class="input-field col s6">
            {{ Form::label('krt2', '2. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt2', $list, $data->krt2) }}
        </div>
        <div class="input-field col s6">
            {{ Form::label('krt3', '3. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt3', $list, $data->krt3) }}
        </div>
        <div class="input-field col s6">
            {{ Form::label('krt4', '4. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt4', $list, $data->krt4) }}
        </div>
        <div class="input-field col s6">
            {{ Form::label('krt5', '5. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt5', $list, $data->krt5) }}
        </div>
        <div class="input-field col s6">
            {{ Form::label('krt6', '6. Kepadatan Lalu Lintas', ['class' => 'active']) }}
            {{ Form::select('krt6', $list, $data->krt6) }}
        </div>
    </div>
    
    {{ Form::close() }}
@stop
@section('js')
$('.collection a:nth-child(3)').addClass('active');
@stop