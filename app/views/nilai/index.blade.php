@extends('default')
@section('body')
<h1>Data Nilai</h1>
<table class="bordered striped ">
    <thead>
        <tr>
            <th data-field="no">No</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="ID Lokasi">ID</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Lalu Lintas">Krt 1</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah SPBU tiap ruas Jalan">Krt 2</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perumahan per Kecamatan">Krt 3</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Jumlah Perkantoran dan Industri">Krt 4</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Kepadatan Penduduk">Krt 5</th>
            <th class="tooltipped" data-position="top" data-delay="30" data-tooltip="Harga Lahan Setempat">Krt 6</th>
            <th>Detail</th>
        </tr>
    </thead>

    <tbody>
    @foreach($nilai as $data)
        <tr>
            <td>{{$no}}</td> <?php $no++ ?>
            <td>{{$data->lokasi->alamat}}</td>
            <td>{{$data->krt1}}</td>
            <td>{{$data->krt2}}</td>
            <td>{{$data->krt3}}</td>
            <td>{{$data->krt4}}</td>
            <td>{{$data->krt5}}</td>
            <td>{{$data->krt6}}</td>
            <td>
                <a href="{{ route('nilaidetail', $data->id) }}" class="waves-effect waves-light btn"><i class="mdi-content-send"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@stop
@section('js')
$('.collection a:nth-child(3)').addClass('active');
@stop