<?php 

class LoginController extends Controller {
    public function login() {
        return View::make('login.index');
    }
    public function home() {
        return View::make('home');
    }
    public function logout() {
        if(Auth::check()) {
          Auth::logout();
        }
        return Redirect::route('home');
    }
	public function create() {
		return View::make('login.create');
	}
    public function all() {
        $user = User::with('roles')->get();
        return View::make('login.list', compact('user')); 
    }
    public function handleLogin() {
        $data = Input::only(['username', 'password']);
        
        $validator = Validator::make(
            $data,
            [
                'username' => 'required|min:5',
                'password' => 'required',
            ]
        );

        if($validator->fails()){
            return Redirect::route('home')->withPesan('Terdapat kesalahan input')->withInput();
        }

        $user = User::where('username', '=', $data['username'])->first();
        if(!$user) {
            return Redirect::route('home')->withPesan('Tidak ditemukan user.')->withInput();
        }

        // auth with plain text
        if(!$user->password == $data['password']) {
        return Redirect::route('home')->withPesan('Password salah.')->withInput();
        }

        Auth::login($user);
        return Redirect::route('filter');

        // auth with password hash
        // if(Auth::attempt(['username' => $data['username'], 'password' => $data['password']])){
        //     return Redirect::route('filter');
        // }
    }
	public function store() {
            // return dd(Input::all());
        $data = Input::all();
        
        $validator = Validator::make(
            $data,
            [
                'name' => 'required|min:5',
                'username'  =>  'required|min:5',
                'password' => 'required|min:6|confirmed',
                'access'    => 'required',
                'file'    => 'required|mimes:jpeg,jpg,gif,bmp,png'
            ]
        );
        if($validator->fails()){
            return Redirect::route('addusers')->withPesan('Data yang anda masukkan salah')->withErrors($validator)->withInput();
        }

        if(User::where('name', '=', $data['name'])->first()) {
            return Redirect::route('addusers')->withPesan('Nama yang anda masukkan tidak boleh sama')->withInput();
        }
        if(User::where('username', '=', $data['username'])->first()) {
            return Redirect::route('addusers')->withPesan('Username yang anda masukkan tidak boleh sama')->withInput();
        }

        $image = $data['file'];
        $fullname = Str::slug(Str::random(8)) . '-' . $data['username'] . '.' . $image->guessClientExtension();
        $upload = $image->move(public_path(). '/img/',$fullname);
        $data['file'] = $fullname;
        // $data['password'] = Hash::make($data['password']);

        $newUser = new User;
        $newUser->name = Input::get('name');
        $newUser->username = Input::get('username');
        $newUser->password = Input::get('password');
        $newUser->file = $fullname;
        $newUser->save();

        if(!$newUser){
            return Redirect::route('addusers')->withPesan('Gagal membuat user baru')->withInput();
        }

        $role = Role::where('name', '=', $data['access'])->first();
        $newUser->attachRole($role);
        if(!$role) {
            return Redirect::route('addusers')->withPesan('Role tidak ditemukan')->withInput();
        }

        Auth::login($newUser);
        return Redirect::route('home');
	}
}