<?php 

use Faker\Factory as Faker;

class NilaiController extends Controller {

	public function index() {
        $no = 1;
       	$nilai = NilaiData::all()->reverse();
    	return View::make('nilai.index')->with('nilai', $nilai)->with('no', $no);
    }
    public function detail($id) {
        $lokasi = Lokasi::find($id);
        $nilai = NilaiData::find($id);
        return View::make('nilai.detail')->with('nilai', $nilai)->with('lokasi', $lokasi);
    }
    
    public function edit($id) {
        $list = [
            1 => 'Bobot 1 Tidak Layak', 
            2 => 'Bobot 2 Kurang Layak',
            3 => 'Bobot 3 Cukup Layak',
            4 => 'Bobot 4 Layak',
            5 => 'Bobot 5 Sangat Layak'
        ];
        $lokasi = Lokasi::find($id);
        $data = NilaiData::find($id);
        return View::make('nilai.ubah', compact('list', 'data', 'lokasi'));
    }

    public function postEdit($id) {
        $data = NilaiData::find($id);
        $data->krt1 = Input::get('krt1');
        $data->krt2 = Input::get('krt2');
        $data->krt3 = Input::get('krt3');
        $data->krt4 = Input::get('krt4');
        $data->krt5 = Input::get('krt5');
        $data->krt6 = Input::get('krt6');
        $data->nilai = $data->krt1 +$data->krt2 +$data->krt3 +$data->krt4 +$data->krt5 +$data->krt6;
        $data->save();
    
        return Redirect::route('nilaidetail', $data->id )->withPesan('Data berhasil di ubah.'); 
    }

    public function hapus($id) {
        Lokasi::find($id)->delete();
        NilaiData::find($id)->delete();
        HasilData::find($id)->delete();
        return Redirect::route('nilai')->withPesan('Data berhasil di hapus.');
    }
}