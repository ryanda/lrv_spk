<?php 

class AhpController extends Controller {

	public function ubah() {
		$list = [
            1 => '1', 
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5'
        ];
        $data = Banding::all();
		return View::make('kriteriaahp.ubah', compact('list', 'data'));
	}
    public function hitung() {

        // start of Matrik Perbandingan Berpasangan
    	$data1 = Banding::findOrFail(1);
    	$data2 = Banding::findOrFail(2);
    	$data3 = Banding::findOrFail(3);
    	$data4 = Banding::findOrFail(4);
    	$data5 = Banding::findOrFail(5);
        $data6 = Banding::findOrFail(6);

    	$data1->k2 = Input::get('21'); $data2->k1 = 1/$data1->k2;
    	$data1->k3 = Input::get('31'); $data3->k1 = 1/$data1->k3;
    	$data1->k4 = Input::get('41'); $data4->k1 = 1/$data1->k4;
    	$data1->k5 = Input::get('51'); $data5->k1 = 1/$data1->k5;
    	$data1->k6 = Input::get('61'); $data6->k1 = 1/$data1->k6;
    	$data2->k3 = Input::get('32'); $data3->k2 = 1/$data2->k3;
    	$data2->k4 = Input::get('42'); $data4->k2 = 1/$data2->k4;
    	$data2->k5 = Input::get('52'); $data5->k2 = 1/$data2->k5;
    	$data2->k6 = Input::get('62'); $data6->k2 = 1/$data2->k6;
    	$data3->k4 = Input::get('43'); $data4->k3 = 1/$data3->k4;
    	$data3->k5 = Input::get('53'); $data5->k3 = 1/$data3->k5;
    	$data3->k6 = Input::get('63'); $data6->k3 = 1/$data3->k6;
    	$data4->k5 = Input::get('54'); $data5->k4 = 1/$data4->k5;
    	$data4->k6 = Input::get('64'); $data6->k4 = 1/$data4->k6;
    	$data5->k6 = Input::get('65'); $data6->k5 = 1/$data5->k6;

    	$data1->save();
    	$data2->save();
    	$data3->save();
    	$data4->save();
    	$data5->save();
        $data6->save();
        // end of Matrik Perbandingan Berpasangan

        // start of Matrik Nilai Kriteria
        $k1 = DB::table('krt_banding')->lists('k1');
        $k2 = DB::table('krt_banding')->lists('k2');
        $k3 = DB::table('krt_banding')->lists('k3');
        $k4 = DB::table('krt_banding')->lists('k4');
        $k5 = DB::table('krt_banding')->lists('k5');
        $k6 = DB::table('krt_banding')->lists('k6');

        $jmlk1 = array_sum($k1);
        $jmlk2 = array_sum($k2);
        $jmlk3 = array_sum($k3);
        $jmlk4 = array_sum($k4);
        $jmlk5 = array_sum($k5);
        $jmlk6 = array_sum($k6);

        $nilai1 = Nilai::findOrFail(1);
        $nilai2 = Nilai::findOrFail(2);
        $nilai3 = Nilai::findOrFail(3);
        $nilai4 = Nilai::findOrFail(4);
        $nilai5 = Nilai::findOrFail(5);
        $nilai6 = Nilai::findOrFail(6);
        
        $nilai1->k1 = $k1[0]/$jmlk1; $nilai1->k2 = $k2[0]/$jmlk2; $nilai1->k3 = $k3[0]/$jmlk3; $nilai1->k4 = $k4[0]/$jmlk4; $nilai1->k5 = $k5[0]/$jmlk5; $nilai1->k6 = $k6[0]/$jmlk6; $nilai1->jml = $nilai1->k1+$nilai1->k2+$nilai1->k3+$nilai1->k4+$nilai1->k5+$nilai1->k6; $nilai1->prt = $nilai1->jml/6;
        $nilai2->k1 = $k1[1]/$jmlk1; $nilai2->k2 = $k2[1]/$jmlk2; $nilai2->k3 = $k3[1]/$jmlk3; $nilai2->k4 = $k4[1]/$jmlk4; $nilai2->k5 = $k5[1]/$jmlk5; $nilai2->k6 = $k6[1]/$jmlk6; $nilai2->jml = $nilai2->k1+$nilai2->k2+$nilai2->k3+$nilai2->k4+$nilai2->k5+$nilai2->k6; $nilai2->prt = $nilai2->jml/6;
        $nilai3->k1 = $k1[2]/$jmlk1; $nilai3->k2 = $k2[2]/$jmlk2; $nilai3->k3 = $k3[2]/$jmlk3; $nilai3->k4 = $k4[2]/$jmlk4; $nilai3->k5 = $k5[2]/$jmlk5; $nilai3->k6 = $k6[2]/$jmlk6; $nilai3->jml = $nilai3->k1+$nilai3->k2+$nilai3->k3+$nilai3->k4+$nilai3->k5+$nilai3->k6; $nilai3->prt = $nilai3->jml/6;
        $nilai4->k1 = $k1[3]/$jmlk1; $nilai4->k2 = $k2[3]/$jmlk2; $nilai4->k3 = $k3[3]/$jmlk3; $nilai4->k4 = $k4[3]/$jmlk4; $nilai4->k5 = $k5[3]/$jmlk5; $nilai4->k6 = $k6[3]/$jmlk6; $nilai4->jml = $nilai4->k1+$nilai4->k2+$nilai4->k3+$nilai4->k4+$nilai4->k5+$nilai4->k6; $nilai4->prt = $nilai4->jml/6;
        $nilai5->k1 = $k1[4]/$jmlk1; $nilai5->k2 = $k2[4]/$jmlk2; $nilai5->k3 = $k3[4]/$jmlk3; $nilai5->k4 = $k4[4]/$jmlk4; $nilai5->k5 = $k5[4]/$jmlk5; $nilai5->k6 = $k6[4]/$jmlk6; $nilai5->jml = $nilai5->k1+$nilai5->k2+$nilai5->k3+$nilai5->k4+$nilai5->k5+$nilai5->k6; $nilai5->prt = $nilai5->jml/6;
        $nilai6->k1 = $k1[5]/$jmlk1; $nilai6->k2 = $k2[5]/$jmlk2; $nilai6->k3 = $k3[5]/$jmlk3; $nilai6->k4 = $k4[5]/$jmlk4; $nilai6->k5 = $k5[5]/$jmlk5; $nilai6->k6 = $k6[5]/$jmlk6; $nilai6->jml = $nilai6->k1+$nilai6->k2+$nilai6->k3+$nilai6->k4+$nilai6->k5+$nilai6->k6; $nilai6->prt = $nilai6->jml/6;

        $nilai1->save(); $nilai2->save(); $nilai3->save(); $nilai4->save(); $nilai5->save(); $nilai6->save();
        // end of Matrik Nilai Kriteria

        // start of Matrik Penjumlahan Tiap Baris
        $jumlah1 = Jumlah::findOrFail(1);
        $jumlah2 = Jumlah::findOrFail(2);
        $jumlah3 = Jumlah::findOrFail(3);
        $jumlah4 = Jumlah::findOrFail(4);
        $jumlah5 = Jumlah::findOrFail(5);
        $jumlah6 = Jumlah::findOrFail(6);

        $jumlah1->k1 = $data1->k1*$nilai1->prt; $jumlah1->k2 = $data1->k2*$nilai1->prt; $jumlah1->k3 = $data1->k3*$nilai1->prt; $jumlah1->k4 = $data1->k4*$nilai1->prt; $jumlah1->k5 = $data1->k5*$nilai1->prt; $jumlah1->k6 = $data1->k6*$nilai1->prt;
        $jumlah2->k1 = $data2->k1*$nilai2->prt; $jumlah2->k2 = $data2->k2*$nilai2->prt; $jumlah2->k3 = $data2->k3*$nilai2->prt; $jumlah2->k4 = $data2->k4*$nilai2->prt; $jumlah2->k5 = $data2->k5*$nilai2->prt; $jumlah2->k6 = $data2->k6*$nilai2->prt;
        $jumlah3->k1 = $data3->k1*$nilai3->prt; $jumlah3->k2 = $data3->k2*$nilai3->prt; $jumlah3->k3 = $data3->k3*$nilai3->prt; $jumlah3->k4 = $data3->k4*$nilai3->prt; $jumlah3->k5 = $data3->k5*$nilai3->prt; $jumlah3->k6 = $data3->k6*$nilai3->prt;
        $jumlah4->k1 = $data4->k1*$nilai4->prt; $jumlah4->k2 = $data4->k2*$nilai4->prt; $jumlah4->k3 = $data4->k3*$nilai4->prt; $jumlah4->k4 = $data4->k4*$nilai4->prt; $jumlah4->k5 = $data4->k5*$nilai4->prt; $jumlah4->k6 = $data4->k6*$nilai4->prt;
        $jumlah5->k1 = $data5->k1*$nilai5->prt; $jumlah5->k2 = $data5->k2*$nilai5->prt; $jumlah5->k3 = $data5->k3*$nilai5->prt; $jumlah5->k4 = $data5->k4*$nilai5->prt; $jumlah5->k5 = $data5->k5*$nilai5->prt; $jumlah5->k6 = $data5->k6*$nilai5->prt;
        $jumlah6->k1 = $data6->k1*$nilai6->prt; $jumlah6->k2 = $data6->k2*$nilai6->prt; $jumlah6->k3 = $data6->k3*$nilai6->prt; $jumlah6->k4 = $data6->k4*$nilai6->prt; $jumlah6->k5 = $data6->k5*$nilai6->prt; $jumlah6->k6 = $data6->k6*$nilai6->prt;

        $jumlah1->jml = $jumlah1->k1+$jumlah1->k2+$jumlah1->k3+$jumlah1->k4+$jumlah1->k5+$jumlah1->k6;  
        $jumlah2->jml = $jumlah2->k1+$jumlah2->k2+$jumlah2->k3+$jumlah2->k4+$jumlah2->k5+$jumlah2->k6; 
        $jumlah3->jml = $jumlah3->k1+$jumlah3->k2+$jumlah3->k3+$jumlah3->k4+$jumlah3->k5+$jumlah3->k6; 
        $jumlah4->jml = $jumlah4->k1+$jumlah4->k2+$jumlah4->k3+$jumlah4->k4+$jumlah4->k5+$jumlah4->k6; 
        $jumlah5->jml = $jumlah5->k1+$jumlah5->k2+$jumlah5->k3+$jumlah5->k4+$jumlah5->k5+$jumlah5->k6; 
        $jumlah6->jml = $jumlah6->k1+$jumlah6->k2+$jumlah6->k3+$jumlah6->k4+$jumlah6->k5+$jumlah6->k6;

        $jumlah1->save(); $jumlah2->save(); $jumlah3->save(); $jumlah4->save(); $jumlah5->save(); $jumlah6->save();
        // end of Matrik Penjumlahan Tiap Baris

        // start of Rasio Konsistensi
        $rasio1 = Rasio::findOrFail(1);
        $rasio2 = Rasio::findOrFail(2);
        $rasio3 = Rasio::findOrFail(3);
        $rasio4 = Rasio::findOrFail(4);
        $rasio5 = Rasio::findOrFail(5);
        $rasio6 = Rasio::findOrFail(6);

        $rasio1->jml = $jumlah1->jml; $rasio1->prt = $nilai1->prt; $rasio1->hsl = $rasio1->jml/$rasio1->prt;
        $rasio2->jml = $jumlah2->jml; $rasio2->prt = $nilai2->prt; $rasio2->hsl = $rasio2->jml/$rasio2->prt;
        $rasio3->jml = $jumlah3->jml; $rasio3->prt = $nilai3->prt; $rasio3->hsl = $rasio3->jml/$rasio3->prt;
        $rasio4->jml = $jumlah4->jml; $rasio4->prt = $nilai4->prt; $rasio4->hsl = $rasio4->jml/$rasio4->prt;
        $rasio5->jml = $jumlah5->jml; $rasio5->prt = $nilai5->prt; $rasio5->hsl = $rasio5->jml/$rasio5->prt;
        $rasio6->jml = $jumlah6->jml; $rasio6->prt = $nilai6->prt; $rasio6->hsl = $rasio6->jml/$rasio6->prt;

        $rasio1->save(); $rasio2->save(); $rasio3->save(); $rasio4->save(); $rasio5->save(); $rasio6->save();

        // end of Rasio Konsistensi

    	return Redirect::route('ahp');
    }
    public function index() {
       	$banding = Banding::all()->toArray();
        $jumlah = Jumlah::all()->toArray();
        $nilai = Nilai::all()->toArray();
        $rasio = Rasio::all()->toArray();

    	return View::make('kriteriaahp.index', compact('banding', 'jumlah', 'nilai', 'rasio'));
    }
}