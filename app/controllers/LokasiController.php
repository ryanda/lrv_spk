<?php 

use Faker\Factory as Faker;

class LokasiController extends Controller {

    public function index() {
        $no = 1;
        $lokasi = Lokasi::all()->reverse();
        // $lokasi = Lokasi::orderByRaw('RAND()')->get();
    	return View::make('lokasi.index', compact('lokasi', 'no'));
    }

    public function edit($id) {
        $lokasi = Lokasi::find($id);
        return View::make('lokasi.ubah', compact('lokasi'));
    }
    public function postEdit($id) {
        $lokasi = Lokasi::find($id);
        $lokasi->noid = Input::get('noid');
        $lokasi->alamat = Input::get('alamat');
        $lokasi->kbp = Input::get('kbp');
        $lokasi->prv = Input::get('prv');
        $lokasi->lat = Input::get('lat');
        $lokasi->lon = Input::get('lon');
        $lokasi->save();
    
        return Redirect::route('lokasidetail', $lokasi->id )->withPesan('Data berhasil di ubah.'); 
    }

    public function detail($id) {
        $lokasi = Lokasi::find($id);
        return View::make('lokasi.detail')->with('lokasi', $lokasi);
    }

    public function tambah() {
        return View::make('lokasi.tambah');
    }

    public function postTambah() {
        $faker = Faker::create();
        $data = Input::only(['alamat', 'kbp', 'prv', 'lat', 'lon']);
        
        $validator = Validator::make(
            $data,
            [
                'alamat' => 'required|min:5',
                'kbp' => 'required',
                'prv' => 'required'
            ]
        );

        if($validator->fails()){
            return Redirect::route('lokasitambah')->withPesan('Terdapat kesalahan input data.')->withInput();
        } else {
            $lokasi = new Lokasi;
            $lokasi->noid = $faker->numerify('####');
            $lokasi->alamat = Input::get('alamat');
            $lokasi->kbp = Input::get('kbp');
            $lokasi->prv = Input::get('prv');
            $lokasi->lat = Input::get('lat');
            $lokasi->lon = Input::get('lon');
            $lokasi->save();

            $nilai = new NilaiData;
            $nilai->lokasi_id = $lokasi->id;
            $nilai->save();

            $hasil = new HasilSaw;
            $hasil->nilai_id = $lokasi->id;
            $hasil->lokasi_id = $lokasi->id;
            $hasil->save();

            $hasil = new HasilAhp;
            $hasil->nilai_id = $lokasi->id;
            $hasil->lokasi_id = $lokasi->id;
            $hasil->save();
        
            return Redirect::to('lokasi')->withPesan('Data berhasil di tambah.');
        }
    }

    public function hapus($id) {
        Lokasi::find($id)->delete();
        NilaiData::find($id)->delete();
        HasilAhp::find($id)->delete();
        HasilSaw::find($id)->delete();
        return Redirect::route('lokasi')->withPesan('Data berhasil di hapus.');
    }
}