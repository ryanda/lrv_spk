<?php 

class SawController extends Controller {

    public function index() {
        $beban = Beban::all();

        return View::make('kriteriasaw.index', compact('beban'));
    }
	public function ubah() {
		$list = [
            1 => '1', 
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6'
        ];
        $jumlah = 0;
        $beban = Beban::all();
		return View::make('kriteriasaw.ubah', compact('list', 'beban', 'jumlah'));
	}
    public function hitung() {

        $beban = Input::get('bbn');
        $jumlah = array_sum($beban);

        if ($jumlah <= 20) {
            return Redirect::route('sawubah')->withPesan('Angka kriteria terlalu Kecil');
        } elseif ($jumlah >= 22) {
            return Redirect::route('sawubah')->withPesan('Angka kriteria terlalu Besar');
        } else {
            $j = 0;
            for($i=1;$i < count($beban)+1;$i++)
            {
                $n = Beban::find($i); 
                $n->beban = $beban[$j];
                $n->save();
                $j++;
            }
            return Redirect::route('saw')->withPesan('Kriteria berhasil diperbarui');
        }
    }
 
}