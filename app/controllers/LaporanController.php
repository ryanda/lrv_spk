<?php 

use Faker\Factory as Faker;

class LaporanController extends Controller {
    
    // general blocks
    public function index() {
        return View::make('laporan.index');
    }
    public function detail($id) {
        $lokasi = Lokasi::find($id);
        $nilai = NilaiData::find($id);
        $hasil = HasilAhp::find($id);
        return View::make('laporan.detail')->with('nilai', $nilai)->with('hasil', $hasil)->with('lokasi', $lokasi);
    }
    public function hapus($id) {
        Lokasi::find($id)->delete();
        NilaiData::find($id)->delete();
        HasilAhp::find($id)->delete();
        HasilSaw::find($id)->delete();
        return Redirect::route('laporan')->withPesan('Data berhasil di hapus.');
    }

    // ahp blocks
    public function indexahp() {
        $no = 1;
        $hasil = HasilAhp::orderBy('nilai', 'DESC')->get();
        return View::make('laporan.ahpindex', compact('hasil','no'));
    }
    public function hitungahp() {
        $nilai = NilaiData::all();
        $hasil = HasilAhp::all();
        $krt = DB::table('krt_nilai')->lists('prt');
        return View::make('laporan.ahphitung', compact('nilai', 'krt', 'hasil'));
    }
    public function postHitungahp() {

        $nilai = NilaiData::all();
        $hasil = HasilAhp::all();
        $krt = DB::table('krt_nilai')->lists('prt');

        $k1 = Input::get('k1');
        $k2 = Input::get('k2');
        $k3 = Input::get('k3');
        $k4 = Input::get('k4');
        $k5 = Input::get('k5');
        $k6 = Input::get('k6');
        $jumlah = Input::get('jumlah');

        $n1 = Input::get('n1');
        $n2 = Input::get('n2');
        $n3 = Input::get('n3');
        $n4 = Input::get('n4');
        $n5 = Input::get('n5');
        $n6 = Input::get('n6');
        $jn = Input::get('jn');

        $j = 0;
        for($i=1;$i < count($k1)+1;$i++)
        {
            $n = NilaiData::find($i); 
            $n->krt1 = $n1[$j];
            $n->krt2 = $n2[$j];
            $n->krt3 = $n3[$j];
            $n->krt4 = $n4[$j];
            $n->krt5 = $n5[$j];
            $n->krt6 = $n6[$j]; 
            $n->nilai = $jn[$j];
            $n->save();

            $h = HasilAhp::find($i); 
            $h->k1 = $k1[$j];
            $h->k2 = $k2[$j];
            $h->k3 = $k3[$j];
            $h->k4 = $k4[$j];
            $h->k5 = $k5[$j];
            $h->k6 = $k6[$j]; 
            $h->nilai = $jumlah[$j];
            $h->save();

            $j++;
        }

        return Redirect::route('laporahp')->withPesan('Perhitungan sudah diperbarui');
    }

    // saw blocks
    public function indexsaw() {
        $no = 1;
        $hasil = HasilSaw::orderBy('nilai', 'DESC')->get();
        return View::make('laporan.sawindex')->with('hasil', $hasil)->with('no', $no);
    }
    public function hitungsaw() {
        $nilai = NilaiData::all();
        $hasil = HasilSaw::all();
        $krt = DB::table('beban')->lists('beban');

        $krt1 = DB::table('nilai')->sum('krt1');
        $krt2 = DB::table('nilai')->sum('krt2');
        $krt3 = DB::table('nilai')->sum('krt3');
        $krt4 = DB::table('nilai')->sum('krt4');
        $krt5 = DB::table('nilai')->sum('krt5');
        $krt6 = DB::table('nilai')->sum('krt6');

        return View::make('laporan.sawhitung', compact('nilai', 'krt', 'hasil','krt1','krt2','krt3','krt4','krt5','krt6' ));
    }
    public function postHitungsaw() {

        $nilai = NilaiData::all();
        $hasil = HasilSaw::all();

        $k1 = Input::get('k1');
        $k2 = Input::get('k2');
        $k3 = Input::get('k3');
        $k4 = Input::get('k4');
        $k5 = Input::get('k5');
        $k6 = Input::get('k6');
        $jumlah = Input::get('jumlah');

        $n1 = Input::get('n1');
        $n2 = Input::get('n2');
        $n3 = Input::get('n3');
        $n4 = Input::get('n4');
        $n5 = Input::get('n5');
        $n6 = Input::get('n6');
        $jn = Input::get('jn');

        $j = 0;
        for($i=1;$i < count($hasil)+1;$i++)
        {
            $n = NilaiData::find($i); 
            $n->krt1 = $n1[$j];
            $n->krt2 = $n2[$j];
            $n->krt3 = $n3[$j];
            $n->krt4 = $n4[$j];
            $n->krt5 = $n5[$j];
            $n->krt6 = $n6[$j]; 
            $n->nilai = $jn[$j];
            $n->save();

            $h = HasilSaw::find($i); 
            $h->k1 = $k1[$j];
            $h->k2 = $k2[$j];
            $h->k3 = $k3[$j];
            $h->k4 = $k4[$j];
            $h->k5 = $k5[$j];
            $h->k6 = $k6[$j]; 
            $h->nilai = $jumlah[$j];
            $h->save();

            $j++;
        }

        return Redirect::route('laporsaw')->withPesan('Perhitungan sudah diperbarui');
    }
}