<?php

use Faker\Factory as Faker;

class TestController extends Controller {

	public function index() {
		// $lokasi = Lokasi::all();
        // $lokasi = Lokasi::orderByRaw('RAND()')->get();
		$lokasi = Lokasi::get();

		// $photos = Photo::all()->take(9);
 
  //       foreach($photos as $photo){
 
  //           $response['photos'][] = [
  //               'id' => $photo->id,
  //               'user_id' => $photo->user_id,
  //               'url' => $photo->url,
  //               'title' => $photo->title,
  //               'description' => $photo->description,
  //               'category' => $photo->category,
  //           ];
  //       }
		
 
	    return Response::json(array(
	        'error' => false,
	        'lokasi' => $lokasi->toArray()),
	        200
	    );
	}

	public function store() {

		 // $url = new Url;
		 //    $url->url = Request::get('url');
		 //    $url->description = Request::get('description');
		 //    $url->user_id = Auth::user()->id;
		 
		 //    // Validation and Filtering is sorely needed!!
		 //    // Seriously, I'm a bad person for leaving that out.
		 
		 //    $url->save();
		 
		 //    return Response::json(array(
		 //        'error' => false,
		 //        'urls' => $urls->toArray()),
		 //        200
		 //    );

		$faker = Faker::create();
        $data = Request::all();
        
        $validator = Validator::make(
            $data,
            [
                'alamat' => 'required|min:5',
                'kbp' => 'required',
                'prv' => 'required'
            ]
        );

        if($validator->fails()){
            return Response::json([
            	'pesan' => 'validasi gagal'
            ], 500);
        } else {
            $lokasi 		= new Lokasi;
            $lokasi->noid 	= $faker->numerify('####');
            $lokasi->alamat = $data['alamat'];
            $lokasi->kbp 	= $data['kbp'];
            $lokasi->prv 	= $data['prv'];
            $lokasi->lat 	= $data['lat'];
            $lokasi->lon 	= $data['lon'];
            $lokasi->save();

            $nilai 				= new NilaiData;
            $nilai->lokasi_id 	= $lokasi->id;
            $nilai->save();

            $hasil 				= new HasilSaw;
            $hasil->nilai_id 	= $lokasi->id;
            $hasil->lokasi_id 	= $lokasi->id;
            $hasil->save();

            $hasil 				= new HasilAhp;
            $hasil->nilai_id 	= $lokasi->id;
            $hasil->lokasi_id 	= $lokasi->id;
            $hasil->save();

            return 'success';
        }

		// Lokasi::create(Request::all())
		// return 'hello';
	}

	public function show($id) {
		$lokasi = Lokasi::find($id);
 
	    return Response::json(array(
	        'error' => false,
	        'lokasi' => $lokasi->toArray()),
	        200
	    );
	}

	public function update($id) {

        $data = Request::json()->all();

		$lokasi = Lokasi::find($id);
        $lokasi->alamat = Request::get('alamat');
        $lokasi->kbp 	= Request::get('kbp');
        $lokasi->prv 	= Request::get('prv');
        $lokasi->lat 	= Request::get('lat');
        $lokasi->lon 	= Request::get('lon');
        // $lokasi->alamat 		= $data['alamat'];
        // $lokasi->kbp 		= $data['kbp'];
        // $lokasi->prv 		= $data['prv'];
        // $lokasi->lat 		= $data['lat'];
        // $lokasi->lon 		= $data['lon'];

        // $lokasi->alamat 	= $data->alamat;
        // $lokasi->kbp 		= $data->kbp;
        // $lokasi->prv 		= $data->prv;
        // $lokasi->lat 		= $data->lat;
        // $lokasi->lon 		= $data->lon;
        $lokasi->save();
	    
	    return Response::json(array(
	        'error' => false,
	        'message' => 'url updated'),
	        200
	    );
		// $url = Url::where('user_id', Auth::user()->id)->find($id);
	 
	 //    if ( Request::get('url') )
	 //    {
	 //        $url->url = Request::get('url');
	 //    }
	 
	 //    if ( Request::get('description') )
	 //    {
	 //        $url->description = Request::get('description');
	 //    }
	 
	 //    $url->save();
	 
	 //    return Response::json(array(
	 //        'error' => false,
	 //        'message' => 'url updated'),
	 //        200
	 //    );
	}

	public function destroy($id) {
		Lokasi::destroy($id);
		return 'success';

		Lokasi::destroy($id);
		Lokasi::find($id)->delete();
        NilaiData::find($id)->delete();
        HasilData::find($id)->delete();
        return Redirect::route('lokasi')->withPesan('Biodata berhasil di hapus.');

     //    $url = Url::where('user_id', Auth::user()->id)->find($id);
 
	    // $url->delete();
	 
	    // return Response::json(array(
	    //     'error' => false,
	    //     'message' => 'url deleted'),
	    //     200
	    //     );
	}


}
