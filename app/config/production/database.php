<?php

return array(
    // 'profile' => false,
    'default' => 'mysql',
    'connections' => array(
      'mysql' => array(
            'driver'    => 'mysql',
            'host'      => $_SERVER['DATABASE1_HOST'],
            'database'  => $_SERVER['DATABASE1_NAME'],
            'username'  => $_SERVER['DATABASE1_USER'],
            'password'  => $_SERVER['DATABASE1_PASS'],
            'port'      => $_SERVER['DATABASE1_PORT'],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
      ),
    ),
);