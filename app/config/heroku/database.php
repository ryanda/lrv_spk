<?php 

$url = parse_url(getenv("DATABASE_URL"));

$host = $url["host"];
$username = $url["user"];
$password = $url["pass"];
$database = substr($url["path"], 1);

return array(

	'default' => 'pgsql',

	'connections' => array(
		'pgsql'	=> array(
			'driver'   => 'pgsql',
			'host'     => $host,
			'database' => $database,
			'username' => $username,
			'password' => $password,
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		),
		'mysql'	=> array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'lrv_si',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'port'		=> '3306'
		),
	),
);