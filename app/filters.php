<?php

// management on role permission
Entrust::routeNeedsRole( 'lokasi*', ['Admin', 'User'], null, false );
Entrust::routeNeedsRole( 'nilai*', ['Admin', 'Manager', 'User'], null, false );
Entrust::routeNeedsRole( 'ahp*', ['Manager'], null, false );
Entrust::routeNeedsRole( 'saw*', ['Manager'], null, false );
Entrust::routeNeedsRole( 'users*', ['Admin', 'Manager'], null, false );
Entrust::routeNeedsRole( 'laporanahp*', ['Manager'], null, false );
Entrust::routeNeedsRole( 'laporansaw*', ['Manager'], null, false );
Entrust::routeNeedsRole( 'laporan*', ['Manager'], null, false );

Route::filter('yadm', function() {
	if(!Entrust::hasRole('Admin')) {
		App::abort(403);
	}
});
Route::filter('nmng', function() {
	if(Entrust::hasRole('Manager')) {
		App::abort(403);
	}
});

// login logout stuff
Route::filter('auth', function() {
    if (Auth::guest()) return Redirect::route('home');
});
Route::filter('guest', function() {
    if (Auth::check()) return Redirect::to('/');
})
;
// for api header
Route::filter('csrf', function() {
	$token = Request::ajax() ? Request::header('X-CSRF-TOKEN') : Input::get('_token');
	if (Session::token() != $token) throw new Illuminate\Session\TokenMismatchException;
});

// single role filter
// Route::filter('admin', function() {
// 	if(! Entrust::hasRole('Admin')) {
// 		App::abort(403);
// 	}
// });

// Route::filter('manager', function() {
// 	if(! Entrust::hasRole('Manager')) {
// 		App::abort(403);
// 	}
// });

// Route::filter('user', function() {
// 	if(! Entrust::hasRole('User')) {
// 		App::abort(403);
// 	}
// });
